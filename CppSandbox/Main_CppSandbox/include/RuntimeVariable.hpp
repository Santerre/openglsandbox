#pragma once

#include <string>
#include "Delegate.hpp"

#include <glm/glm.hpp>

#include "ObjectHandle.hpp"

class Object;

class RuntimeVariable
{
public:
	enum struct Type : int
	{
		DOUBLE = 0,
		STRING,
		INT,
		BOOL,
		VEC3,
		VEC4,
		OBJECT,
		UNKNOWN
	};

	RuntimeVariable() = default;

#pragma region TYPE_GETTER
	/// Returns the enum value Type corresponding to template parameter T.
	/// templateSpecializer is used to make the function 
	/// less likely to be called compared to no params overloads,
	/// so it can be ignored completely when using the function.
	template <typename T, typename... Args>
	static Type GetTypeEnum(Args...)
	{
		static_assert(sizeof(T) == -1, "Unknown template type");
	}

	/// Ill formed if the type T isn't derived from Object.
	/// Used in situations where SFINAE rule applies to discard overloads
	/// from being chosen if Object is not a base class of T.
	template<typename T, typename ReturnType>
	using SFINAE_IsHeritedFromObject =
		typename std::enable_if<
			std::is_base_of<
				Object,
				typename std::remove_pointer<
					typename std::remove_reference<T>::type>::type>::value,
		ReturnType>::type;

	//DEFINE_GET_TYPE_ENUM_FUNC(Object*, OBJECT)
	template <typename T>
	static SFINAE_IsHeritedFromObject<T, Type> GetTypeEnum()
	{
		return Type::OBJECT;
	}

#pragma endregion TYPE_GETTER

	template<typename T>
	static RuntimeVariable* Allocate(std::string const& name, T const& value, int templateSpecializer = 0)
	{
		RuntimeVariable* rVar = new RuntimeVariable();
		rVar->SetName(name);
		rVar->SetType(GetTypeEnum<T>());
		rVar->SetValue(value, true);
		return rVar;
	}
	template<typename T>
	static RuntimeVariable* Allocate(std::string const& name, T& value, int templateSpecializer = 0)
	{
		RuntimeVariable* rVar = new RuntimeVariable();
		rVar->SetName(name);
		rVar->SetType(GetTypeEnum<T>());
		rVar->SetValue<T>(value, true);
		return rVar;
	}
	
	template<typename T>
	static RuntimeVariable* Allocate(std::string const& name, SFINAE_IsHeritedFromObject<T, T> value)
	{
		RuntimeVariable* rVar = new RuntimeVariable();
		rVar->SetName(name);
		rVar->SetType(GetTypeEnum<T>());
		rVar->SetValue<T>(value, true);
		rVar->DoInheritsFromType = [](Object* obj) -> bool
		{
			return dynamic_cast<T>(obj);
		};
		return rVar;
	}

	virtual ~RuntimeVariable();

	std::string const&	GetName() const { return _name; }
	void				SetName(std::string const& name) { _name = name; }

	Type				GetType() const { return _type; }
	void				SetType(Type type) { _type = type; }

	int					GetIntValue() const;
	bool				GetBoolValue() const;
	double				GetDoubleValue() const;
	std::string			GetStringValue() const;
	glm::vec3			GetVec3Value() const;
	glm::vec4			GetVec4Value() const;
	ObjectHandle		GetObjectHandleValue() const;
	Object*				GetObjectValue() const;

	template<typename T, typename... Args>
	T const& GetValue(Args...) const
	{
		if (_data)
			return *reinterpret_cast<T*>(_data);
		else
			return T();
	}

	template<typename T>
	SFINAE_IsHeritedFromObject<T, T> GetValue() const
	{
		return static_cast<T>(GetObjectValue());
	}

	void const*			GetRawValue() const;

	/// templateSpecializer is used to make the function 
	/// less likely to be called compared to no params overloads,
	/// so it can be ignored completely when using the function.
	template<typename T, typename... Args>
	void SetValue(T const& value, bool ignoreChange = false, Args...)
	{
		if (_data)
		{
			delete _data;
			_data = nullptr;
		}
		_data = new T(value);
		_size = sizeof(T);

		if (!ignoreChange)
			OnValueChanged.Invoke();
	}

	/// For the object case, wrap it in an object handle 
	/// to be able to retrieve the object threw different engine running sessions.
	template<typename T>
	SFINAE_IsHeritedFromObject<T, void> SetValue(T value, bool ignoreChange = false)
	{
		if (_data)
		{
			delete _data;
			_data = nullptr;
		}
		_data = new ObjectHandle(value);
		_size = sizeof(ObjectHandle);

		if (!ignoreChange)
			OnValueChanged.Invoke();
	}

	Delegate<void>  OnValueChanged;
	
	void			CopyDataFrom(RuntimeVariable const& value);

	bool			HideInEditor = false;
	float			DisplayPrecision = 0.01f;
	float			DisplayMin = -10000.0f;
	float			DisplayMax = 100000.f;

	std::function<bool(Object*)> DoInheritsFromType;

protected:
	std::string		_name;
	Type			_type = Type::UNKNOWN;
	void*			_data = nullptr;
	int				_size = 0;

};

#define DECLARE_GET_TYPE_ENUM_FUNC(ctype, enumType)\
	template <>\
	RuntimeVariable::Type RuntimeVariable::GetTypeEnum<ctype>();

DECLARE_GET_TYPE_ENUM_FUNC(int, INT)
DECLARE_GET_TYPE_ENUM_FUNC(double, DOUBLE)
DECLARE_GET_TYPE_ENUM_FUNC(std::string, STRING)
DECLARE_GET_TYPE_ENUM_FUNC(bool, BOOL)
DECLARE_GET_TYPE_ENUM_FUNC(glm::vec3, VEC3)
DECLARE_GET_TYPE_ENUM_FUNC(glm::vec4, VEC4)
#undef DECLARE_GET_TYPE_ENUM_FUNC
