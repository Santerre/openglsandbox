#pragma once
#include "Asset.hpp"

class AstImage :
	public Asset
{
public:
	AstImage(std::string const& assetPath);
	virtual ~AstImage();
};

