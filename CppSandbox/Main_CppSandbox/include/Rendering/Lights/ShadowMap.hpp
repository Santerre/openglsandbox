#pragma once

class ShadowMap
{
public:
	ShadowMap();
	virtual ~ShadowMap();

	static ShadowMap* CreateShadowMap();
	static void DestroyShadowMap(ShadowMap* shadowMap);
	virtual void InitDepthMap() = 0;

	void SetBias(float bias) { _bias = bias; }
	float GetBias() const { return _bias; }

	int GetResX() const { return _depthMapRes[0]; }
	int GetResY() const { return _depthMapRes[1]; }

protected:
	float _bias;
	int	  _depthMapRes[2] = { 4096, 4096 };

};

