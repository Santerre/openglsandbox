#pragma once

#include "Object.hpp"

class GameObject;
class Shader;
class DrawContextData;

/// The material is used to render a specified target object.
class Material :
	public Object
{
public:
	Material(std::string const& name, GameObject* attached);
	virtual ~Material();

	virtual void		Initialize() {}

	/// Render the material.
	virtual void		Render(DrawContextData* data, int pass) {}
	/// Render the material for shadow mapping. Only depth will be used.
	virtual void		RenderShadows() {}

	virtual bool		IsValid();

	void				SetShader(Shader* shader, int index = 0);
	Shader*				GetShader(int index = 0) const { return _shaders[index]; };

	void				SetShadowShader(Shader* shadowShader) { _shadowShader = shadowShader; };
	Shader*				GetShadowShader() const { return _shadowShader; };

	GameObject*			GetTargetGO() { return _targetGO; }

	RVAR(bool,			DoDrawShadows, true);

	static const int	kMaxPass = 10;
protected:
	Shader* _shaders[kMaxPass] = {};
	Shader* _shadowShader = nullptr;

	GameObject* _targetGO = nullptr;
};
