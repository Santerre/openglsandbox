#pragma once

#include "GameObject.hpp"

class RenderingContext;
class GameObject;

class RenderingPipeline
{
public:
	RenderingPipeline() = default;
	virtual ~RenderingPipeline() = default;

	virtual void			Initialize() = 0;
	virtual void			Render(RenderingContext* context, bool debugRenderer) = 0;
	virtual void			Shutdown() = 0;

protected:
	template <typename ForwardIt>
	void PreRenderObjects(
		ForwardIt first,
		ForwardIt last)
	{
		GameObject* go;
		for (auto it = first; it != last; it++)
		{
			go = (*it)->GetTargetGO();
			if (go)
				go->PreRender();
		}
	}
};

