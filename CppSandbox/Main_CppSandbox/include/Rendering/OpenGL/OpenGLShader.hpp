#pragma once

#include "Rendering/Shader.hpp"

#include <gl/glew.h>
#include <map>

class OpenGLShader
	: public Shader
{
public:
	GLuint GetProgram() const { return _program; }

	virtual bool LoadShader(const char* path, ShaderType shaderType) override;
	virtual void Compile() override;
	virtual void Use(DrawContextData* contextData) const override;

private:
	GLenum GetOpenGLShaderType(ShaderType shaderType);

	GLuint _program;
	std::map<ShaderType, GLuint> linkedShaders;
};