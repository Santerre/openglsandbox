#pragma once

#include <string>
#include <atomic>

enum struct AsyncLoadResult
{
	CANCELED,
	PENDING,
	DONE,
};

class Texture
{
public:

	virtual bool LoadImage(std::string const& path, bool linearSpace, bool isHDR = false) = 0;
	virtual bool LoadImage(void* data, int width, int height, bool linearSpace, bool isHDR = false) = 0;

	virtual bool LoadCubemap(std::string const& path) = 0;

	/// Prepare the image in an asynchrone way and store the result in a variable,
	/// waiting LoadPreparedImageAsync to be called from the main thread.
	/// The function does not create any thread, used to be called from user thread.
	virtual AsyncLoadResult PrepareImageAsync(std::string const& path, int* textureXY) = 0;
	/// Load the image loaded async.
	virtual bool	LoadPreparedImageAsync(int width, int height, bool linearSpace) = 0;
	/// Clear any asynchrone loading.
	virtual void	DropLoadAsync() = 0;
	/// Get if an asynchrone load is in progress.
	bool			IsLoadAsyncReady();

	bool SafeLoadImage(std::string const& path, bool linearSpace, bool isHDR = false);
	bool LoadNullImage(int sizeX, int sizeY, bool isHDR = false);

protected:
	int _width = 0;
	int _height = 0;

	/// The number of time the texture loading is retried in multithreaded context.
	const size_t _retryFileLoadingCount = 10;

	std::atomic<int> _asyncLoadingID{ -1 };
	std::atomic<void*> _asyncLoadRes{ nullptr };

private:

};