#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

#include "Rendering/Materials/ACubemapMaterialTemplate.hpp"

class DrawContextData;

class CubemapMaterial :
	public ACubemapMaterialTemplate
{
public:
	CubemapMaterial(std::string const& name, GameObject* attached);
	~CubemapMaterial();

	virtual void	Render(DrawContextData* data, int pass) override;

private:
	GLuint _vbo;
	GLuint _vao;
};

