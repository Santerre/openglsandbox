#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Rendering/Materials/APBRMaterialTemplate.hpp"
#include "Rendering/Resources/Mesh.hpp"

class DrawContextData;

class PBRMaterial :
	public APBRMaterialTemplate
{
public:
	PBRMaterial(std::string const& name, GameObject* attached);
	virtual ~PBRMaterial();

	virtual void Initialize() override;

	virtual void RenderShadows() override;
	virtual void Render(DrawContextData* data, int pass) override;

private:
	void BindTexture(GLint location, Texture* texture, GLint textureIndex) const;

	GLuint _vao;
};
