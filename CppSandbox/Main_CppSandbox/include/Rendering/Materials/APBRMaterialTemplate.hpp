#pragma once

#include <string>

#include "Rendering/Material.hpp"
#include "Rendering/Resources/Texture.hpp"
#include "AstImage.hpp"

class GameObject;
class Mesh;

class APBRMaterialTemplate :
	public Material
{
public:
	APBRMaterialTemplate(std::string const& name, GameObject* attached);
	virtual ~APBRMaterialTemplate();

	virtual void Initialize() override;
	virtual void DebugUpdate() override;
	void UpdateTextures();

	void SetMesh(Mesh* mesh) { _mesh = mesh; }

	void SetModel(glm::mat4 const& model) { _model = model; }

	Texture* LoadTextureAtPath(AstImage* img, bool linearSpace) const;

	RVAR(AstImage*, Albedo, nullptr);
	RVAR(AstImage*, Specular, nullptr);
	RVAR(AstImage*, Gloss, nullptr);
	RVAR(AstImage*, Normal, nullptr);

protected:
	glm::mat4		_model;

	Mesh*			_mesh;

	Texture*		_texAlbedo;
	Texture*		_texSpecular;
	Texture*		_texGlossiness;
	Texture*		_texNormal;
};

