#pragma once
#include "Asset.hpp"
class AstModel :
	public Asset
{
public:
	AstModel(std::string const& assetPath);
	virtual ~AstModel();
};

