#pragma once

#include <thread>
#include <atomic>
#include <mutex>
#include <chrono>
#include <condition_variable>

#include "Rendering/Resources/Texture.hpp"

class ResourceThread
{
public:
	ResourceThread();

	template <typename FN, typename... Args>
	void Run(FN func, Args... args)
	{
		thread = new std::thread(&ResourceThread::LoadResourceAsync<FN, Args...>, this, func, args...);
	}

	~ResourceThread();

	template <typename FN, typename... Args>
	void LoadResourceAsync(FN func, Args... args)
	{
		{
			std::unique_lock<std::mutex> guard(canLoad);
			conditionVariable.wait(guard, []() { return threadWorkingCount < 5; });
			threadWorkingCount++;
		}
		conditionVariable.notify_one();

		result = func(args...);
		threadWorkingCount--;
		conditionVariable.notify_one();
	}

	void Wait();
	bool IsLoadSuccess();
	bool IsLoadDone();

	bool InstallLoadedResource();

	Texture* Texture = nullptr;
	bool LinearSpace;
	int TextureSizeXY[2] = {};

private:
	std::thread*					thread = nullptr;
	std::atomic<AsyncLoadResult>	result{ AsyncLoadResult::PENDING };

	static std::mutex				canLoad;
	static std::atomic<int>			threadWorkingCount;
	static std::condition_variable	conditionVariable;
};

