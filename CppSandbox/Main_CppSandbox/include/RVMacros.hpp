#pragma once

#include "RVHandle.hpp"
#include "PersistentBoard.hpp"

//class PersistentBoard;

/// Create a new runtime variable and add it to the defined board, 
/// then define a new handle.
#define RVAR_BOARD(type, name, value, board) \
RVHandle<type> name = AddNewRVToPersistendBoard<type>(board, #name, value);

template<typename T>
static RVHandle<T> AddNewRVToPersistendBoard(PersistentBoard* board, std::string name, T defaultValue)
{
	auto res = board->AddVal(RuntimeVariable::Allocate<T>(name, defaultValue));
	Assert(res);
	return RVHandle<T>(res);
}
