#pragma once

#include <random>

#define RANDOM Random::Get()

class Random
{
public:
	~Random();

	static Random& Get();

	void Initialize();

	/// Return a randomly generated number 
	/// following the specified gaussian distribution.
	float GetGaussianSample(float mean, float sDev);

	/// Fill outData with length randomly generated numbers
	/// following the specified gaussian distribution.
	/// Note that outData must have at least length size.
	void GaussianNoiseArray(float* outData, size_t length, float mean, float sDev);

	std::default_random_engine generator;

private:
	Random();
	Random(Random const&) = delete;
	Random(Random&&) = delete;

	Random& operator=(Random const&) = delete;
	Random& operator=(Random&&) = delete;
};

