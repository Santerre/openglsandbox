#pragma once

#include <glm/glm.hpp>

// GLFW
#include <GLFW/glfw3.h>

namespace OpenGLUtils
{
	template <typename T>
	void AddGLMUniform(GLuint program, char const* name, T const& value, bool mustExist = true)
	{
		GLuint location = glGetUniformLocation(program, name);
		if (mustExist)
			Assert(location != -1);
		AddGLMUniform(location, value);
	}

	void AddGLMUniform(GLint location, int value);
	void AddGLMUniform(GLint location, float value);
	void AddGLMUniform(GLint location, glm::vec2 const& value);
	void AddGLMUniform(GLint location, glm::vec3 const& value);
	void AddGLMUniform(GLint location, glm::vec4 const& value);
	void AddGLMUniform(GLint location, glm::mat4 const& value);
} // OpenGLUtils
