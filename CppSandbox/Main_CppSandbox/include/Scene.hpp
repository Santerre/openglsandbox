#pragma once

#include "assimp/scene.h"

class Scene
{
public:
	Scene(aiScene const* scene);
	~Scene();

	aiScene const* GetAiScene() const { return _scene; }

private:
	aiScene const* _scene;
};

