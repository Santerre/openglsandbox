#pragma once
#include "Asset.hpp"
class AstShader :
	public Asset
{
public:
	AstShader(std::string const& assetPath);
	virtual ~AstShader();
};

