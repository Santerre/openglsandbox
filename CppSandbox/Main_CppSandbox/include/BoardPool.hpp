#pragma once

#include <string>
#include <memory>
#include <map>

class TiXmlDocument;
class PersistentBoard;

class BoardPool
{
public:
	BoardPool();
	~BoardPool();

	void GenerateDocument();
	void GenerateDocument(TiXmlDocument& document) const;
	bool SaveDocument(std::string const& fileName);

	bool Load(const char* name);

	void UpdatePersistantBoard(std::string const & keyName, PersistentBoard * board, bool ignoreDirty = false);

	void Dirty();
	bool IsDirty() { return _isDirty; }

private:
	void AddBoard(std::string const & key, PersistentBoard * board, bool ignoreDirty);

	bool _isDirty = false;
	std::map<std::string, PersistentBoard*> _pool;
	std::unique_ptr<TiXmlDocument> _document;
};

