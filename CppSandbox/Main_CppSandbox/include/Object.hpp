#pragma once

#include <string>
#include "RVMacros.hpp"

class PersistentBoard;

/// Create a runtime variable of type type, add it to the persistent board with the default value value, 
/// and declare a member handle named name.
/// Substitute of the declaration of a type with the notation :
/// type name = value;
/// ex : 
/// int i = 45; 
/// become
/// RVAR(int, i, 45);
/// This can only work in a Object based class declaration.
#define RVAR(type, name, value) RVAR_BOARD(type, name, value, this->_persistentBoard)

class Object
{
public:
	Object(std::string const& name);
	virtual ~Object();

	Object(Object& obj) = delete;
	Object(Object&& obj) = delete;
	Object& operator=(Object const&) = delete;
	Object& operator=(Object&&) = delete;

	virtual void DebugUpdate();
	void _InternalInit();
	virtual void Update() {};

	bool IsInitialized() const { return _initialized; }
	bool IsDebugAuto() const { return _debugAuto; }

	std::string const& GetName() { return _name; }

	PersistentBoard* GetPersistantBoard() { return _persistentBoard; } 
	PersistentBoard const* GetPersistantBoard() const { return _persistentBoard; } 

	virtual std::string			GetFullName() { return _name; };

protected:
	virtual void				Initialize() {}

	bool						_debugAuto = true;
	bool						_initialized = false;
	std::string					_name;
	PersistentBoard*			_persistentBoard = nullptr;
};

