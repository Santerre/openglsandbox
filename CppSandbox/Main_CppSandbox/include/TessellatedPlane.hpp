#pragma once

#include "Rendering/Resources/Mesh.hpp"

/// Represent a plane with the specified tessellation level.
class TessellatedPlane :
	public Mesh
{
public:
	TessellatedPlane();
	/// Generate immediatelly the mesh with the specified tessellation level.
	/// auto plane = new TessellatedPlane(45);
	/// Is Equivalent to :
	/// auto plane = new TessellatedPlane();
	/// plane->UpdateMesh(45);
	TessellatedPlane(unsigned int tessellationLevel);

	~TessellatedPlane();

	/// Update the mesh for the terrain.
	/// Wait for the GameObject main buffers (vao, vbo, ibo) ready to fill.
	/// tesselationLevel represent the number of square on an edge.
	/// returns false if any error happened while updating mesh. Error means undefined behavior with the vbo/ibo.
	bool UpdateMesh(unsigned int tesselationLevel);

private:
	unsigned int _currentTessellation = 0;
};

