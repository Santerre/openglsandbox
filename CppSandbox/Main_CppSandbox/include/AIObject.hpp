#pragma once

#include "GameObject.hpp"

#include "AstImage.hpp"

#include "Rendering/Materials/PBRMaterial.hpp"

class AIObject :
	public GameObject
{
public:
	AIObject(GameObject* parent, aiNode const* node);
	~AIObject();

	aiNode const* GetAiNode() const
	{
		return _node;
	}

	virtual void Initialize() override;
	virtual void PreRender() override;

private:
	void			LoadMesh(aiMesh const* mesh);

	Mesh*			_mesh = nullptr;

	void			GenIndicesBuffer(aiMesh const* mesh, uint32_t*& buffer, int& count) const;
	aiMaterial*		GetAIMaterial(int meshIndex = 0) const;

	aiNode const*	_node;
};

