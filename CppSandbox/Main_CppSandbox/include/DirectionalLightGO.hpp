#pragma once

#include "LightGO.hpp"
#include <string>
#include "glm/glm.hpp"

class ShadowMap;

class DirectionalLightGO final:
	public LightGO
{
public:
	DirectionalLightGO(std::string const& name);
	~DirectionalLightGO();

	virtual void	DebugUpdate() override;
	virtual void	Update() override;

	glm::vec3 GetDirection() const { return _persistentBoard->QuickGetVec3Val("Direction"); }
	glm::mat4 GetViewProjection() const;
	void SetDirection(glm::vec3 const& val) { _persistentBoard->QuickSetVal("Direction", val); }

	bool DoDrawShadows() const { return _persistentBoard->QuickGetBoolVal("DrawShadows"); }

	ShadowMap* GetShadowMap() const { return _shadowMap; }

	RVAR(double, ShadowBias, 0.005);

private :
	ShadowMap* _shadowMap = nullptr;
};

