#pragma once

#include "glm/glm.hpp"
#include "Object.hpp"
#include "PersistentBoard.hpp"
#include "RVHandle.hpp"

class LightGO :
	public Object
{
public:
	LightGO(std::string const& name);
	virtual ~LightGO();

	virtual void DebugUpdate() override;

	glm::vec4 GetColor() const { return _persistentBoard->QuickGetVec4Val("Color"); }

	RVAR(double, Intensity, 1.0);
};

