#pragma once

class Shader;

class ShaderDesc
{
public:
	ShaderDesc(char const* shaderName);
	~ShaderDesc();

	void LoadShader(char const* vertexShader, char const* fragmentShader);
	/// Will handle the destruction of the shader.
	void SetShader(Shader* shader);
	bool IsValid();

	Shader const*	GetShader() const { return _shader; }
	char const*		GetName() const { return _name; }

private:
	char const*		_name = "";

	Shader*			_shader = nullptr;
};

