#pragma once

#include "Debug.hpp"
#include "RuntimeVariable.hpp"

template <typename T>
class RVHandle
{
public:
	RVHandle(RuntimeVariable* rv)
		: _rv(rv)
	{
	}

	RVHandle()
	{
	}

	~RVHandle() = default;

	void SetRV(RuntimeVariable* rv)
	{
		_rv = rv;
	}

	RuntimeVariable const& GetRV() const
	{
		return *_rv;
	}

	RuntimeVariable& GetRV()
	{
		return *_rv;
	}

	operator T() const 
	{
		Assert(IsValid());
		return _rv->GetValue<T>(); 
	}

	T const& GetValue() const
	{
		return _rv->GetValue<T>();
	}

	/*typename std::remove_pointer<T>::type const& operator*() const
	{
		Assert(IsValid());
		return *_rv->GetValue<T>();
	}*/

	T operator =(T const& value)
	{ 
		Assert(IsValid());
		_rv->SetValue(value); 
		return _rv->GetValue<T>();
	}

	RVHandle& operator=(const RVHandle&) = delete;

	inline bool IsValid() const { return _rv != nullptr; }

private:
	RuntimeVariable* _rv = nullptr;
};

