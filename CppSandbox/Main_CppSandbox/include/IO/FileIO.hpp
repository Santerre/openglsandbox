#pragma once

#include <vector>
#include <string>

namespace FileIO
{
	/// Return all the forders in a direcory. 
	/// If the folder cannot be openned, return empty vector.
	std::vector<std::string> GetFoldersInDirectory(const char* path);
	/// Return all the files in a direcory. 
	/// If the folder cannot be openned, return empty vector.
	std::vector<std::string> GetFilesInDirectory(const char* path);

	bool FileExists(const char* path);

	std::string GetExtention(std::string const& file);
	std::string ChangeExtention(std::string const& file, std::string const& ext);
	std::string GetName(std::string const& file);

	std::string TransformToCurrentPlateform(std::string const& path);
}