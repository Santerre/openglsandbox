#pragma once

#include "IO/Input.hpp"

struct GLFWwindow;

class GLFWInput :
	public Input
{
public:
	GLFWInput();
	GLFWInput(GLFWInput const&) = delete;
	GLFWInput(GLFWInput&&) = delete;
	~GLFWInput();

	GLFWInput& operator=(GLFWInput const&) = delete;
	GLFWInput& operator=(GLFWInput&&) = delete;

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Shutdown() override;
	virtual void DoHideCursor(bool doHideCursor) override;

private:
	void UpdateKey(InputAction& action, int key, int bindedKey, int keyAction);

	// Function prototypes
	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
	static void MouseCallback(GLFWwindow* window, double xpos, double ypos);
	static void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
	static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	static void CharCallback(GLFWwindow* window, unsigned int c);

	bool		_resetMouse = false;
	double		_mouseSensitivity = 0.05;
	bool		_moveInputs[6] = { false };
};

