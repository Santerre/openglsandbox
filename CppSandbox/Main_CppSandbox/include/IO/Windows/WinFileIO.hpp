#pragma once

#include <vector>
#include <string>

namespace WinFileIO
{
	/// Return all the forders in a direcory. 
	/// If the folder cannot be openned, return empty vector.
	std::vector<std::string> GetFoldersInDirectory(const char* path);

	/// Return all the files in a direcory. 
	/// If the folder cannot be openned, return empty vector.
	std::vector<std::string> GetFilesInDirectory(const char* path);

	bool FileExists(const char* path);

} // namespace WinFileIO