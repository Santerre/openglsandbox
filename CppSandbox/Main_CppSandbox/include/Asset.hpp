#pragma once

#include <string>
#include "Object.hpp"

class Asset
: public Object
{
public:

	enum struct Type
	{
		UNKNOWN = 0,
		IMAGE,
		SHADER,
		MODEL,
	};

							Asset(Type type, std::string const& assetPath);
	virtual 				~Asset();

	inline Type				GetType() { return _type; }
	virtual std::string		GetFullName() override;
	inline std::string		GetPath() {	return _assetPath; }

	static Type				StringToType(std::string const& type);
	static std::string		TypeToString(Type type);

private:
	Type					_type;
	std::string				_assetPath;
};
