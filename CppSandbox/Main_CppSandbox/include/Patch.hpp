#pragma once
#include "GameObject.hpp"

class Patch :
	public GameObject
{

public:
	Patch(std::string const& name, GameObject* parent);
	~Patch();

//	virtual void Render(Shader const* shader, int pass) override;
	virtual void Update() override;

protected:
	virtual void Initialize() override;

private:
	float* PointsToFloatArray() const;


	void UpdateIBO();
	void UpdateIBO(int pointCount);
//	GLuint GetIndexCount() const;
//	GLuint GetIndexCount(int pointCount) const;

	static constexpr int GetIndex(int x, int y) { return x * _pointX + y; }
	static const int _pointX = 4;
	static const int _pointY = 4;
	static const int _pointCount = _pointX * _pointY;
};

