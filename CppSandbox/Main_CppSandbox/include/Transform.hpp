#pragma once
#include <glm/glm.hpp>
#include "Delegate.hpp"

/// Hierarchy agnostic transformations.
class Transform
{
public:
	Transform();
	~Transform();

	void SetPosition(glm::vec3 const& newPos);
	void SetTransformMatrix(glm::mat4 const& mat);

	glm::vec3 GetPosition() const;
	glm::mat4 const& GetTransformMatrix() const;

	Delegate<void, Transform const&> OnTransformChanged;

private:
	glm::mat4x4					_transformMat = glm::mat4();
};

