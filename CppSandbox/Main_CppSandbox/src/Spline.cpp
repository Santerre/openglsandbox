#include "Spline.hpp"

#include "Rendering/Shader.hpp"
#include "imgui.h"

#include "Rendering/Resources/Mesh.hpp"

Spline::Spline(std::string const& name, GameObject* parent)
: GameObject(name, parent)
{
	_persistentBoard->AddVal(RuntimeVariable::Allocate("DrawPoints", false));

	_persistentBoard->AddVal(RuntimeVariable::Allocate("Tesselation", 20));
	_persistentBoard->AddVal(RuntimeVariable::Allocate("BSplineLoop", false));
	auto splineTypeValue = RuntimeVariable::Allocate("SplineType", 0);
	splineTypeValue->HideInEditor = true;
	_persistentBoard->AddVal(splineTypeValue);

	for (int i = 0; i < _pointCount; i++)
	{
		auto pointVar = RuntimeVariable::Allocate("Point" + std::to_string(i + 1), glm::vec3(i, 0, 0));
		_persistentBoard->AddVal(pointVar);
		pointVar->DisplayPrecision = 0.3;
		pointVar->DisplayMin = -100;
		pointVar->DisplayMax = 100;
	}

	RAssert(_pointCount >= 4);

	_mesh = new Mesh();

	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	glBufferData(GL_ARRAY_BUFFER, _pointCount * 3 * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _mesh->GetIBO());

	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
}

Spline::~Spline()
{
	delete _mesh;
}

void Spline::Initialize()
{
	GameObject::Initialize();
	UpdateIBO();
}

void Spline::Update()
{
	GameObject::Update();
}

void Spline::DebugUpdate()
{
	GameObject::DebugUpdate();

	int intType = _persistentBoard->QuickGetIntVal("SplineType");
	const char* types[2] = { "Bezier", "BSpline" };
	if (ImGui::Combo("Spline type", &intType, types, 2))
	{
		_persistentBoard->QuickSetVal("SplineType", intType);
		UpdateIBO((SplineType)intType, _pointCount);
	}
}

void Spline::UpdateIBO()
{
	UpdateIBO((SplineType)_persistentBoard->QuickGetIntVal("SplineType"), _pointCount);
}

void Spline::UpdateIBO(SplineType type, int pointCount)
{
	GLuint* iboArray = new GLuint[GetIndexCount(type, pointCount)];

	if (pointCount < 4)
	{
		for (int i = 0; i < pointCount; i++)
			iboArray[i] = i;
	}
	else if (type == Bezier)
	{
		int j = 0;
		for (int i = 0; i < pointCount - 1; i++)
		{
			iboArray[j++] = i;
			if (i % 4 == 3)
				iboArray[j++] = i;
		}
		iboArray[j] = pointCount - 1;
	}
	else if (type == BSpline)
	{
		int realPointCount = pointCount + (_persistentBoard->QuickGetBoolVal("BSplineLoop") ? 3 : 0);
		int j = 0;
		for (int i = 0; i < realPointCount; i++)
		{
			if (i + 3 < realPointCount)
				for (int k = 0; k < 4; k++)
					iboArray[j++] = (i + k) % pointCount;
		}
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _mesh->GetIBO());
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, GetIndexCount(type, pointCount) * sizeof(GLuint), iboArray, GL_STATIC_DRAW);
	delete[] iboArray;
}

GLuint Spline::GetIndexCount() const
{
	return GetIndexCount((SplineType)_persistentBoard->QuickGetIntVal("SplineType"), _pointCount);
}
GLuint Spline::GetIndexCount(SplineType type, int pointCount) const
{
	if (pointCount < 4)
		return pointCount;
	switch (type)
	{
		case Bezier:
			return ((pointCount - 1) / 3) * 4;
		case BSpline:
			return (pointCount - 3 + (_persistentBoard->QuickGetBoolVal("BSplineLoop") ? 3 : 0)) * 4;
	}
	return -1;
}

void Spline::Render(Shader const* shader, int pass)
{
	if (!_mesh)
		return;

	if (pass == 1)
	{
		if (_persistentBoard->QuickGetBoolVal("DrawPoints"))
		{
			glPointSize(10);
			glDrawArrays(GL_POINTS, 0, _pointCount);
			if (GetSplineType() == SplineType::Bezier)
				glDrawArrays(GL_LINES, 0, _pointCount);
			else
				glDrawArrays(GL_LINE_STRIP, 0, _pointCount);
			glPointSize(1);
		}
		return;
	}

	glUniform1i(glGetUniformLocation(shader->Program, "splineType"), GetSplineType());

	glLineWidth(4);
	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	float* array = PointsToFloatArray();
	glBufferData(GL_ARRAY_BUFFER, _pointCount * 3 * sizeof(float), array, GL_DYNAMIC_DRAW);
	delete[] array;
	glBindVertexArray(_vao);
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	float values[] = { 10, 10 };
	glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, values);
	int tessLevel = _persistentBoard->QuickGetIntVal("Tesselation");
	float values2[] = { tessLevel, tessLevel, 0, 0};
	glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, values2);
	glDrawElements(GL_PATCHES, GetIndexCount(), GL_UNSIGNED_INT, 0);
	glLineWidth(1);
}

float* Spline::PointsToFloatArray() const
{
	float* result = new float[_pointCount * 3];
	for (int i = 0; i < _pointCount; i++)
	{
		auto value = _persistentBoard->QuickGetVec3Val("Point" + std::to_string(i+1));
		result[i * 3] = value.x;
		result[i * 3 + 1] = value.y;
		result[i * 3 + 2] = value.z;
	}
	return result;
}
