#include "Terrain.hpp"

#include "glm/glm.hpp"
#include <glm/gtc/type_ptr.hpp>

#include "Rendering/Shader.hpp"
#include "Game.hpp"
#include "AstImage.hpp"
#include "TessellatedPlane.hpp"
#include "Tools/OpenGL/OpenGLUtils.hpp"


Terrain::Terrain(std::string const& name, GameObject* parent)
: GameObject(name, parent)
{
	OriginalTesselationLevel.GetRV().OnValueChanged.AddListener(
	[this]() 
	{ 
		if (_mesh)
			static_cast<TessellatedPlane*>(_mesh)->UpdateMesh(OriginalTesselationLevel);
	});	

	HeightMap.GetRV().OnValueChanged.AddListener(
	[this]() 
	{ 
		_heightMap = GAME.resourceManager.LoadTextureAsync(HeightMap.GetValue()->GetPath().c_str(), true);
	});

	TextureUp.GetRV().OnValueChanged.AddListener(
	[this]()
	{
		_textureUp = GAME.resourceManager.LoadTextureAsync(TextureUp.GetValue()->GetPath().c_str(), false);
	});
	TextureDown.GetRV().OnValueChanged.AddListener(
		[this]()
	{
		_textureDown = GAME.resourceManager.LoadTextureAsync(TextureDown.GetValue()->GetPath().c_str(), false);
	});
	TextureCliff.GetRV().OnValueChanged.AddListener(
		[this]()
	{
		_textureCliff = GAME.resourceManager.LoadTextureAsync(TextureCliff.GetValue()->GetPath().c_str(), false);
	});
}

Terrain::~Terrain()
{
	if (_mesh)
		delete _mesh;
}

void Terrain::Initialize()
{
	GameObject::Initialize();
	LoadRenderer();
}

void Terrain::LoadRenderer()
{
	RAssert(LoadMesh(static_cast<GLuint>(OriginalTesselationLevel)));

	/// Textures
	_textureUp = GAME.resourceManager.SafeLoadTexture(TextureUp.GetValue() ? TextureUp.GetValue()->GetPath().c_str() : "", false);
	_textureDown = GAME.resourceManager.SafeLoadTexture(TextureDown.GetValue() ? TextureDown.GetValue()->GetPath().c_str() : "", false);
	_textureCliff = GAME.resourceManager.SafeLoadTexture(TextureCliff.GetValue() ? TextureCliff.GetValue()->GetPath().c_str() : "", false);
	_heightMap = GAME.resourceManager.SafeLoadTexture(HeightMap.GetValue() ? HeightMap.GetValue()->GetPath().c_str() : "", true);

	/// Buffers
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _mesh->GetIBO());
	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, nullptr);
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
}

bool Terrain::LoadMesh(GLuint tesselationLevel)
{
	TessellatedPlane* _tesPlane = new TessellatedPlane();
	_mesh = _tesPlane;
	return _tesPlane->UpdateMesh(tesselationLevel);
}

void Terrain::Render(Shader const* shader, int pass)
{
	GameObject::Render(shader, pass);

	if (!_mesh)
	{
		ELog("Cannot display terrain mesh : mesh not created.");
		return;
	}

	/// Model matrix
	auto modelLoc = glGetUniformLocation(shader->Program, "model");
	glm::mat4 worldTransform = GetWorldTransformation();
	float size = static_cast<float>(Size.GetValue());
	float height = static_cast<float>(Height.GetValue());
	glm::mat4 terrainTransform = glm::mat4(
		size,					0.f,					0.f,					0.f,
		0.f,					height,					0.f,					0.f,
		0.f,					0.f,					size,					0.f,
		worldTransform[0][3],	worldTransform[1][3],	worldTransform[2][3],	1.f);
	glUniformMatrix4fv(modelLoc, 1, false, glm::value_ptr(terrainTransform));

	auto textureTiling = glGetUniformLocation(shader->Program, "textureTiling");
	glUniform1i(textureTiling, TextureTiling);

	auto slopeUpCosLoc = glGetUniformLocation(shader->Program, "slopeUpCos");
	glUniform1f(slopeUpCosLoc, static_cast<float>(SlopeUpCos));

	auto slopeDownCosLoc = glGetUniformLocation(shader->Program, "slopeDownCos");
	glUniform1f(slopeDownCosLoc, static_cast<float>(SlopeDownCos));

	auto upDownBlendLoc = glGetUniformLocation(shader->Program, "upDownBlend");
	glUniform1f(upDownBlendLoc, static_cast<float>(UpDownBlend));

	auto cliffUpTexBlendLoc = glGetUniformLocation(shader->Program, "cliffUpTexBlend");
	glUniform1f(cliffUpTexBlendLoc, static_cast<float>(CliffUpTexBlend));

	auto cliffDownTexBlendLoc = glGetUniformLocation(shader->Program, "cliffDownTexBlend");
	glUniform1f(cliffDownTexBlendLoc, static_cast<float>(CliffDownTexBlend));

	auto normUpValueLoc = glGetUniformLocation(shader->Program, "normUpValue");
	glUniform1f(normUpValueLoc, static_cast<float>(NormUpValue));

	OpenGLUtils::AddGLMUniform(shader->Program, "heightMapTiling", HeightMapTiling);

	auto normalPrecisionLoc = glGetUniformLocation(shader->Program, "normalPrecision");
	glUniform1f(normalPrecisionLoc, static_cast<float>(NormalPrecision) / 1000.f);

	auto tessellationRatioLoc = glGetUniformLocation(shader->Program, "tessellationRatio");
	glUniform1f(tessellationRatioLoc, static_cast<float>(TessellationRatio));

	/// Textures binding
	auto heightmapLoc = glGetUniformLocation(shader->Program, "heighMap");
	auto albedoUpLoc = glGetUniformLocation(shader->Program, "albedoUp");
	auto albedoDownLoc = glGetUniformLocation(shader->Program, "albedoDown");
	auto albedoCliffLoc = glGetUniformLocation(shader->Program, "albedoCliff");

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _heightMap);
	glUniform1i(heightmapLoc, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _textureUp);
	glUniform1i(albedoUpLoc, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _textureDown);
	glUniform1i(albedoDownLoc, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, _textureCliff);
	glUniform1i(albedoCliffLoc, 3);

	/// Array buffer
	glBindVertexArray(_vao);
	glEnableVertexAttribArray(0);

	if (Wireframe.GetValue())
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	/// Draw
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	glDrawElements(GL_PATCHES, _mesh->GetIndicesCount(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//glDisableVertexAttribArray(0);
}