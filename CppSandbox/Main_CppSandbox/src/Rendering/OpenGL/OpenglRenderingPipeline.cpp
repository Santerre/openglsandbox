#include "Rendering/OpenGL/OpenGLRenderingPipeline.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <functional>

#include "imgui.h"
#include "Rendering/OpenGL/OpenGLShader.hpp"

#include "Game.hpp"
#include "ShaderDesc.hpp"
#include "Debug.hpp"
#include "PersistentBoard.hpp"
#include "Rendering/Material.hpp"

#include "Tools/OpenGL/OpenGLUtils.hpp"

#include "Rendering/RenderingContext.hpp"
#include "Rendering/OpenGL/OpenGLRenderingContext.hpp"
#include "Rendering/Resources/OpenGL/OpenGLTexture.hpp"
#include "Rendering/Materials/PBRMaterial.hpp"
#include "Rendering/Materials/CubemapMaterial.hpp"
#include "Rendering/Lights/OpenGL/OpenGLShadowMap.hpp"

OpenGLRenderingPipeline::OpenGLRenderingPipeline()
{
}


OpenGLRenderingPipeline::~OpenGLRenderingPipeline()
{
	glDeleteFramebuffers(1, &framebuffer);
	glDeleteVertexArrays(1, &dummyVAO);
}

void OpenGLRenderingPipeline::Initialize()
{
	glViewport(0, 0, RenderingContext::sGetWidth(), RenderingContext::sGetHeight());

/*	ShaderDesc* desc = new ShaderDesc("Specular");
	desc->LoadShader("Assets/Shaders/specular.vs", "Assets/Shaders/specular.fs");
	auto shader = desc->GetShader();
	if (shader)
	{
		glUseProgram(shader->GetProgram());
		glBindAttribLocation(shader->GetProgram(), 0, "position");
	}
	AddShader(desc);
	blinnShader = GetShaderIndex("Specular");

	desc = new ShaderDesc("ShadowMap");
	desc->LoadShader("Assets/Shaders/shadowmap.vs", "Assets/Shaders/empty.fs");
	AddShader(desc);
	shadowmapShader = GetShaderIndex("ShadowMap");

	desc = new ShaderDesc("CookTorrance");
	desc->LoadShader("Assets/Shaders/cookTorrance.vs", "Assets/Shaders/cookTorrance.fs");
	AddShader(desc);
	cookTorranceShader = GetShaderIndex("CookTorrance");
	
	{
		desc = new ShaderDesc("Spline");
		OpenGLShader* newSplineShader = new OpenGLShader("Assets/Shaders/spline.vs", "Assets/Shaders/spline.fs", false);
		newSplineShader->LoadShader("Assets/Shaders/spline.tes", GL_TESS_EVALUATION_SHADER);
		newSplineShader->Compile();
		desc->SetShader(newSplineShader);
		AddShader(desc);
		splineShader = GetShaderIndex("Spline");
		
		desc = new ShaderDesc("SplineDebug");
		desc->LoadShader("Assets/Shaders/spline_debug.vs", "Assets/Shaders/spline_debug.fs");
		AddShader(desc);
		splineDebugShader = GetShaderIndex("SplineDebug");
	}
	{
		desc = new ShaderDesc("Patch");
		OpenGLShader* newPatchShader = new OpenGLShader("Assets/Shaders/spline.vs", "Assets/Shaders/spline.fs", false);
		newPatchShader->LoadShader("Assets/Shaders/patch.tes", GL_TESS_EVALUATION_SHADER);
		newPatchShader->Compile();
		desc->SetShader(newPatchShader);
		AddShader(desc);
		patchShader = GetShaderIndex("Patch");
	}
	{
		desc = new ShaderDesc("Terrain");
		OpenGLShader* newShader = new OpenGLShader("Assets/Shaders/terrain.vs", "Assets/Shaders/terrain.fs", false);
		newShader->LoadShader("Assets/Shaders/terrain.tcs", GL_TESS_CONTROL_SHADER);
		newShader->LoadShader("Assets/Shaders/terrain.tes", GL_TESS_EVALUATION_SHADER);
		newShader->Compile();
		desc->SetShader(newShader);
		AddShader(desc);
		terrainShader = GetShaderIndex("Terrain");
	}
	{
		desc = new ShaderDesc("Water");
		OpenGLShader* newShader = new OpenGLShader("Assets/Shaders/water.vs", "Assets/Shaders/water.fs", false);
		newShader->LoadShader("Assets/Shaders/terrain.tcs", GL_TESS_CONTROL_SHADER);
		newShader->LoadShader("Assets/Shaders/terrain.tes", GL_TESS_EVALUATION_SHADER);
		newShader->Compile();
		desc->SetShader(newShader);
		AddShader(desc);
		waterShader = GetShaderIndex("Water");
	}

	desc = new ShaderDesc("FinalFixPP");
	desc->LoadShader("Assets/Shaders/simpleQuad.vs", "Assets/Shaders/finalFixPP.fs");
	AddShader(desc);
	worldPostProcess = GetShaderIndex("FinalFixPP");
*/
	ShaderDesc* desc = new ShaderDesc("FinalFixPP");
	desc->LoadShader("Assets/Shaders/simpleQuad.vs", "Assets/Shaders/finalFixPP.fs");
	worldPostProcessShader = desc->GetShader();

	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	GLsizei contextWH[2] = { static_cast<GLsizei>(OpenGLRenderingContext::sGetWidth()), static_cast<GLsizei>(OpenGLRenderingContext::sGetHeight()) };
	glGenTextures(1, &colorAttachment);
	glBindTexture(GL_TEXTURE_2D, colorAttachment);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, contextWH[0], contextWH[1], 0, GL_RGBA, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorAttachment, 0);

	glGenTextures(1, &dsAttachment);
	glBindTexture(GL_TEXTURE_2D, dsAttachment);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, contextWH[0], contextWH[1], 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, dsAttachment, 0);
	Assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glGenVertexArrays(1, &dummyVAO);
	glBindVertexArray(dummyVAO);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	Assert(glGetError() == GL_NO_ERROR);
}

void OpenGLRenderingPipeline::Shutdown()
{
	glDeleteTextures(1, &colorAttachment);
	glDeleteTextures(1, &dsAttachment);
	glDeleteVertexArrays(1, &dummyVAO);
	glDeleteFramebuffers(1, &framebuffer);
}

void OpenGLRenderingPipeline::AddCameraNearFarInfo(OpenGLShader const& shader, Camera const& camera) const
{
	using namespace OpenGLUtils;
	GLint nearLoc = glGetUniformLocation(shader.GetProgram(), "cameraNear");
	GLint farLoc = glGetUniformLocation(shader.GetProgram(), "cameraFar");
	if (nearLoc != -1)
		AddGLMUniform(nearLoc, static_cast<float>(camera.Near));
	if (farLoc != -1)
		AddGLMUniform(farLoc,  static_cast<float>(camera.Far));

}

void OpenGLRenderingPipeline::AddLightsInfo(OpenGLShader const& shader) const
{
	using namespace OpenGLUtils;
	GLint dirLightLocs[] =
	{
		glGetUniformLocation(shader.GetProgram(), "directionalLight.direction"),
		glGetUniformLocation(shader.GetProgram(), "directionalLight.color"),
		glGetUniformLocation(shader.GetProgram(), "directionalLight.intensity"),
	};

	GLint ambiantLightLocs[] =
	{
		glGetUniformLocation(shader.GetProgram(), "ambiantLight.color"),
		glGetUniformLocation(shader.GetProgram(), "ambiantLight.intensity")
	};

	auto directional = GAME.directional;
	auto ambiant = GAME.ambiant;

	AddGLMUniform(dirLightLocs[0], directional->GetDirection());
	AddGLMUniform(dirLightLocs[1], directional->GetColor());
	AddGLMUniform(dirLightLocs[2], static_cast<float>(directional->Intensity));
	AddGLMUniform(ambiantLightLocs[0], ambiant->GetColor());
	AddGLMUniform(ambiantLightLocs[1], static_cast<float>(ambiant->Intensity));

	glActiveTexture(GL_TEXTURE20);
	OpenGLShadowMap* shadowMap = static_cast<OpenGLShadowMap*>(directional->GetShadowMap());
	VRAssert(shadowMap);
	glBindTexture(GL_TEXTURE_2D, shadowMap->GetDepthMap());
	glUniform1i(glGetUniformLocation(shader.GetProgram(), "directionalDepthMap"), 20);

	AddGLMUniform(glGetUniformLocation(shader.GetProgram(), "directionalViewProjection"), directional->GetViewProjection());
	auto shadowBiasLoc = glGetUniformLocation(shader.GetProgram(), "shadowBias");
	glUniform1f(shadowBiasLoc, static_cast<GLfloat>(shadowMap->GetBias()));
}

void OpenGLRenderingPipeline::ComputeLightShadowMap(DirectionalLightGO const& dLight, std::list<Material*> const& shadedMaterials) const
{
	using namespace OpenGLUtils;
	OpenGLShadowMap* shadowMap = static_cast<OpenGLShadowMap*>(dLight.GetShadowMap());
	VRAssert(shadowMap);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowMap->GetFramebuffer());

	glViewport(0, 0, static_cast<GLsizei>(shadowMap->GetResX()), static_cast<GLsizei>(shadowMap->GetResY()));
	glClearColor(1.f, 1.f, 1.f, 1.f);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_FRONT);
	glClear(GL_DEPTH_BUFFER_BIT);

	auto res = dLight.DoDrawShadows();

	if (dLight.DoDrawShadows())
	{
		for (auto&& mat : shadedMaterials)
		{
			Assert(mat);
			if (!mat)
				continue;

			auto shadowShader = static_cast<OpenGLShader*>(mat->GetShadowShader());
			if (shadowShader && mat->DoDrawShadows)
			{
				shadowShader->Use({});
				glUniformMatrix4fv(glGetUniformLocation(shadowShader->GetProgram(), "VP"), 1, false, glm::value_ptr(dLight.GetViewProjection()));
				mat->RenderShadows();
			}
		}
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	Assert(glGetError() == GL_NO_ERROR);
	glCullFace(GL_BACK);
}

void OpenGLRenderingPipeline::DrawMaterials(
	std::vector<Material*> const&						materials, 
	std::function<void(Material const&, OpenGLShader const&)>	preDrawFunc,
	int													passIndex) const
{
	for (auto material : materials)
	{
		Assert(material);
		if (!material)
			continue;
		auto shader = static_cast<OpenGLShader*>(material->GetShader(passIndex));
		if (shader)
		{
			glUseProgram(shader->GetProgram());
			preDrawFunc(*material, *shader);
			material->Render({}, passIndex);
		}
	}
}

void OpenGLRenderingPipeline::RenderSpecular(
	std::vector<Material*> const&	materials, 
	Camera const&					camera, 
	GLuint							skyboxTexture) const
{
	using namespace OpenGLUtils;


	// Setup opengl context
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// Camera/View transformation
	glm::mat4 view, projection;
	camera.GetViewProjection(view, projection);

	DrawMaterials(materials, [this, &view, &projection, &camera, skyboxTexture](Material const& mat, OpenGLShader const& shader)
	{
		auto viewLoc = glGetUniformLocation(shader.GetProgram(), "view");
		auto projLoc = glGetUniformLocation(shader.GetProgram(), "projection");
		GLint viewPos = glGetUniformLocation(shader.GetProgram(), "viewPos");

		// Pass the matrices to the shader
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		AddGLMUniform(viewPos, GAME.camera->pos);

		AddCameraNearFarInfo(shader, *GAME.camera);
		AddLightsInfo(shader);

		glActiveTexture(GL_TEXTURE10);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexture);
		glUniform1i(glGetUniformLocation(shader.GetProgram(), "cubemap"), 10);
	});
}

void OpenGLRenderingPipeline::RenderCubeMap(
	std::vector<Material*> const&	materials, 
	Camera const&					camera) const
{
	using namespace OpenGLUtils;
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	// Camera/View transformation
	glm::mat4 view, projection;
	camera.GetViewProjection(view, projection);
	DrawMaterials(materials, [&view, &projection, &camera](Material const& mat, OpenGLShader const& shader)
	{
		AddGLMUniform(0, projection * glm::mat4(glm::mat3(view)));
	});
	Assert(glGetError() == GL_NO_ERROR);
}

void OpenGLRenderingPipeline::RenderSpline(
	std::vector<Material*> const&	materials, 
	Camera const&					camera) const
{
	using namespace OpenGLUtils;
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// Camera/View transformation
	glm::mat4 view, projection;
	camera.GetViewProjection(view, projection);

	DrawMaterials(materials, [&view, &projection, &camera](Material const& mat, OpenGLShader const& shader)
	{
		glUseProgram(shader.GetProgram());
		auto viewProjectionLoc = glGetUniformLocation(shader.GetProgram(), "viewProjection");
		AddGLMUniform(viewProjectionLoc, projection * view);
	});

	//glDisable(GL_DEPTH_TEST);
	DrawMaterials(materials, [&view, &projection, &camera](Material const& mat, OpenGLShader const& shader)
	{
		auto viewProjectionLoc = glGetUniformLocation(shader.GetProgram(), "viewProjection");
		AddGLMUniform(viewProjectionLoc, projection * view);
	}, 1);
}

void OpenGLRenderingPipeline::RenderPatch(
	std::vector<Material*> const&	materials, 
	Camera const&					camera) const
{
	using namespace OpenGLUtils;
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// Camera/View transformation
	glm::mat4 view, projection;
	camera.GetViewProjection(view, projection);

	DrawMaterials(materials, [&view, &projection, &camera](Material const& mat, OpenGLShader const& shader)
	{
		auto viewProjectionLoc = glGetUniformLocation(shader.GetProgram(), "viewProjection");
		AddGLMUniform(viewProjectionLoc, projection * view);
	}, 0);

	DrawMaterials(materials, [&view, &projection, &camera](Material const& mat, OpenGLShader const& shader)
	{
		auto viewProjectionLoc = glGetUniformLocation(shader.GetProgram(), "viewProjection");
		AddGLMUniform(viewProjectionLoc, projection * view);
	}, 1);
}

void OpenGLRenderingPipeline::RenderTerrain(
	std::vector<Material*> const&	materials, 
	Camera const&					camera) const
{
	using namespace OpenGLUtils;
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// Camera/View transformation
	glm::mat4 view, projection;
	camera.GetViewProjection(view, projection);


	DrawMaterials(materials, [this, &view, &projection, &camera](Material const& mat, OpenGLShader const& shader)
	{
		auto viewProjectionLoc = glGetUniformLocation(shader.GetProgram(), "viewProjection");
		auto viewLocationLoc = glGetUniformLocation(shader.GetProgram(), "viewLocation");
		AddGLMUniform(viewProjectionLoc, projection * view);
		AddGLMUniform(viewLocationLoc, camera.pos);

		AddCameraNearFarInfo(shader, *GAME.camera);
		AddLightsInfo(shader);
		Assert(glGetError() == GL_NO_ERROR);
	});
}

void OpenGLRenderingPipeline::RenderWater(
	std::vector<Material*> const&	materials, 
	Camera const&					camera, 
	GLuint							skyboxTexture) const
{
	using namespace OpenGLUtils;
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// Camera/View transformation
	glm::mat4 view, projection;
	camera.GetViewProjection(view, projection);

	DrawMaterials(materials, [this, &view, &projection, &camera, skyboxTexture](Material const& mat, OpenGLShader const& shader)
	{
		auto viewProjectionLoc = glGetUniformLocation(shader.GetProgram(), "viewProjection");
		auto viewLocationLoc = glGetUniformLocation(shader.GetProgram(), "viewLocation");
		AddGLMUniform(viewProjectionLoc, projection * view);
		AddGLMUniform(viewLocationLoc, camera.pos);

		glActiveTexture(GL_TEXTURE10);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexture);
		glUniform1i(glGetUniformLocation(shader.GetProgram(), "cubemap"), 10);

		AddCameraNearFarInfo(shader, *GAME.camera);
		AddLightsInfo(shader);
		Assert(glGetError() == GL_NO_ERROR);
	});
}

void OpenGLRenderingPipeline::Render(
	RenderingContext*		context, 
	bool					debugRender)
{
	OpenGLRenderingContext* openGLContext = dynamic_cast<OpenGLRenderingContext*>(context);
	RAssert(openGLContext);

	using namespace OpenGLUtils;
	PreRenderObjects(context->GetMaterials().begin(), context->GetMaterials().end());
	ComputeLightShadowMap(*GAME.directional, context->GetMaterials());

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
	glViewport(0, 0, context->GetWidth(), context->GetHeight());

	// Clear the colorbuffer
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	auto* camera = GAME.camera;

	auto cubeMapMaterials = context->GetMaterialsOfType<CubemapMaterial, Material>();
	size_t cubeMapObjCount = cubeMapMaterials.size();
	CubemapMaterial* cubeMapMaterial = nullptr;
	Assert(cubeMapMaterials.size() <= 1);
	if (cubeMapObjCount > 0)
	{
		RenderCubeMap(cubeMapMaterials, *camera);
		cubeMapMaterial = static_cast<CubemapMaterial*>(cubeMapMaterials[0]);
	}

	Texture* cubemapTex = cubeMapMaterial ? cubeMapMaterial->GetCubemapTexture() : nullptr;
	GLuint cubemapTexture = cubemapTex ? static_cast<OpenGLTexture*>(cubemapTex)->GetTexture() : 0;
	Assert(cubemapTexture);
	RenderSpecular(context->GetMaterialsOfType<PBRMaterial, Material>(), *camera, cubemapTexture);

//	shader = GetShader(splineShader);
//	auto debugShader = GetShader(splineDebugShader);
//	if (shader)
//		RenderSpline(GetMaterialsOfType<SplineMaterial, Material>(), *camera);
//
//	shader = GetShader(patchShader);
//	if (shader)
//		RenderSpline(GetMaterialsOfType<PatchMaterial, Material>(), *camera);
//
//	shader = GetShader(terrainShader);
//	if (shader)
//		RenderTerrain(GetMaterialsOfType<TerrainMaterial, Material>(), *camera);
//
//	shader = GetShader(waterShader);
//	if (shader)
//		RenderWater(GetMaterialsOfType<WaterMaterial, Material>(), *camera, cubemapTexture);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	auto shader = static_cast<OpenGLShader const*>(worldPostProcessShader);
	if (shader)
		glUseProgram(shader->GetProgram());
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_FRAMEBUFFER_SRGB);

	int exposureLocation = glGetUniformLocation(shader->GetProgram(), "exposure");
	glUniform1f(exposureLocation, static_cast<float>(GAME.camera->GetPersistantBoard()->QuickGetDoubleVal("Exposure")));

	int fogDensityLocation = glGetUniformLocation(shader->GetProgram(), "fogDensity");
	glUniform1f(fogDensityLocation, static_cast<float>(GAME.camera->FogDensity));

	int fogColorLocation = glGetUniformLocation(shader->GetProgram(), "fogColor");
	AddGLMUniform(fogColorLocation, GAME.camera->FogColor);

	AddCameraNearFarInfo(*shader, *GAME.camera);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// textures
	glUniform1i(glGetUniformLocation(shader->GetProgram(), "tex"), 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, colorAttachment);

	glUniform1i(glGetUniformLocation(shader->GetProgram(), "depthStencilMap"), 1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, dsAttachment);

	glBindVertexArray(dummyVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisable(GL_FRAMEBUFFER_SRGB);

	if (debugRender)
		ImGui::Render();
	// Swap the screen buffers
	glfwSwapBuffers(openGLContext->GetGLFWWindow());

	auto error = glGetError();
	Assert(!error);
}
