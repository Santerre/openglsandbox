#include "Rendering/OpenGL/OpenGLShader.hpp"

#include "Debug.hpp"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

bool OpenGLShader::LoadShader(const char* path, ShaderType shaderType)
{
	// 1. Retrieve the vertex/fragment source code from filePath
	std::string code;
	std::ifstream file;

	// Open files
	file.open(path);
	if (!file.is_open())
		RELog(false, "Couldn't read shader %s", path);

	std::stringstream vShaderStream, fShaderStream;
	// Read file's buffer contents into streams
	vShaderStream << file.rdbuf();
	// close file handlers
	file.close();
	// Convert stream into string
	code = vShaderStream.str();

	const GLchar* shaderCode = code.c_str();
	// 2. Compile shader
	GLuint shader;
	GLint success;
	GLchar infoLog[512];
	// Vertex Shader
	shader = glCreateShader(GetOpenGLShaderType(shaderType));
	glShaderSource(shader, 1, &shaderCode, NULL);
	glCompileShader(shader);
	// Print compile errors if any
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::COMPILATION_FAILED - " << path << "\n" << infoLog << std::endl;
	}
	else
	{
		if (linkedShaders.find(shaderType) != linkedShaders.end())
			glDeleteShader(linkedShaders[shaderType]);
		linkedShaders[shaderType] = shader;
	}

	return success;
}

// Uses the current shader
void OpenGLShader::Use(DrawContextData* contextData) const
{
	glUseProgram(GetProgram());
}

void OpenGLShader::Compile()
{
	// Shader Program
	_program = glCreateProgram();
	for (auto&& shader : linkedShaders)
		glAttachShader(GetProgram(), shader.second);
	glLinkProgram(GetProgram());

	GLint success;
	GLchar infoLog[512];
	// Print linking errors if any
	glGetProgramiv(GetProgram(), GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(GetProgram(), 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
}

GLenum OpenGLShader::GetOpenGLShaderType(ShaderType shaderType)
{
	switch (shaderType)
	{
	case ShaderType::Vertex:
		return GL_VERTEX_SHADER;
	case ShaderType::Fragment:
		return GL_FRAGMENT_SHADER;
	case ShaderType::Compute:
		return GL_COMPUTE_SHADER;
	case ShaderType::TessControl:
		return GL_TESS_CONTROL_SHADER;
	case ShaderType::TessEvaluation:
		return GL_TESS_EVALUATION_SHADER;
	case ShaderType::Geometry:
		return GL_GEOMETRY_SHADER;
	default:
		return 0;
	}
}