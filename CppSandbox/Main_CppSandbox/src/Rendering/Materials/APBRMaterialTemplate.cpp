#include "Rendering/Materials/APBRMaterialTemplate.hpp"

#include "NormalizedDouble.h"
#include "Game.hpp"
#include "imgui.h"

#include "Rendering/Shader.hpp"

APBRMaterialTemplate::APBRMaterialTemplate(std::string const& name, GameObject* attached)
:	Material(name, attached)
{
	_persistentBoard->AddVal(new NormalizedDouble("Metallic", 0.5));
	_persistentBoard->AddVal(new NormalizedDouble("Roughness", 0.5));
	_persistentBoard->AddVal(RuntimeVariable::Allocate("EnvironmentSampleCount", 20));
	_persistentBoard->AddVal(RuntimeVariable::Allocate("Wireframe", false));

	{
#ifndef _ORBIS
		auto shader = Shader::CreateShader("Assets/Shaders/shadowmap.vs", "Assets/Shaders/empty.fs");
		SetShadowShader(shader);
#else
		WLog("Shadows Not implemented.");
#endif //_ORBIS
	}
	{
		auto shader = Shader::CreateShader("Assets/Shaders/cookTorrance.vs", "Assets/Shaders/cookTorrance.fs");
		SetShader(shader, 0);
	}
}

APBRMaterialTemplate::~APBRMaterialTemplate()
{
}

void APBRMaterialTemplate::Initialize()
{
	Material::Initialize();
	UpdateTextures();
}

void APBRMaterialTemplate::DebugUpdate()
{
	if (_mesh)
	{
		Material::DebugUpdate();
		if (ImGui::Button("UpdateTextures"))
			UpdateTextures();
	}
}

Texture* APBRMaterialTemplate::LoadTextureAtPath(AstImage* img, bool linearSpace) const
{
	return GAME.resourceManager.LoadTextureAsync(img->GetPath(), linearSpace);
}

void APBRMaterialTemplate::UpdateTextures()
{
	if (Albedo)
		_texAlbedo = LoadTextureAtPath(Albedo, false);
	if (Specular)
		_texSpecular = LoadTextureAtPath(Specular, true);
	if (Gloss)
		_texGlossiness = LoadTextureAtPath(Gloss, true);
	if (Normal)
		_texNormal = LoadTextureAtPath(Normal, true);
}