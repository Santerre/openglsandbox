#include "Rendering/Materials/OpenGL/OpenGLCubemapMaterial.hpp"

#include "Rendering/Shader.hpp"
#include "Rendering/Resources/OpenGL/OpenGLTexture.hpp"
#include "Game.hpp"
#include "ResourceManager.hpp"

#include "Rendering/OpenGL/OpenGLShader.hpp"

CubemapMaterial::CubemapMaterial(std::string const& name, GameObject* attached)
: ACubemapMaterialTemplate(name, attached)
{
	glGenBuffers(1, &_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);

	glBufferData(GL_ARRAY_BUFFER, verticesCount * 5 * sizeof(GLfloat), cubeVertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 5 * sizeof(GLfloat), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, false, 5 * sizeof(GLfloat), (GLfloat*)0 + 3);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	Assert(glGetError() == GL_NO_ERROR);
}


CubemapMaterial::~CubemapMaterial()
{
}

void CubemapMaterial::Render(DrawContextData* data, int pass)
{
	ACubemapMaterialTemplate::Render(data, pass);

	OpenGLShader* shader = static_cast<OpenGLShader*>(GetShader(pass));
	VRAssert(shader);
	if (_cubemapTex)
	{
		glActiveTexture(GL_TEXTURE0);
		auto cubemapTexture = static_cast<OpenGLTexture*>(_cubemapTex)->GetTexture();
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		auto v2 = glGetUniformLocation(shader->GetProgram(), "cubemapSampler");
		glUniform1i(glGetUniformLocation(shader->GetProgram(), "cubemapSampler"), 0);
		glUseProgram(shader->GetProgram());
		glBindVertexArray(_vao);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		Assert(glGetError() == GL_NO_ERROR);
	}
}
