#include "Rendering/Materials/OpenGL/OpenGLPBRMaterial.hpp"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Rendering/OpenGL/OpenGLShader.hpp"
#include "Debug.hpp"

#include "Rendering/Resources/OpenGL/OpenGLTexture.hpp"
#include "Rendering/Resources/OpenGL/OpenGLMesh.hpp"

PBRMaterial::PBRMaterial(std::string const& name, GameObject* attached)
	: APBRMaterialTemplate(name, attached)
{
}

PBRMaterial::~PBRMaterial()
{
	glDeleteVertexArrays(1, &_vao);
}

void PBRMaterial::Initialize()
{
	APBRMaterialTemplate::Initialize();

	/// VAO
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	OpenGLMesh* mesh = static_cast<OpenGLMesh*>(_mesh);
	if (mesh)
	{
		glBindBuffer(GL_ARRAY_BUFFER, mesh->GetVBO());
		unsigned int vertexSize = mesh->GetVerticesCount();
		glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(Vertex), (GLfloat*)0 + 3);
		glVertexAttribPointer(2, 4, GL_FLOAT, false, sizeof(Vertex), (GLfloat*)0 + 6);
		glVertexAttribPointer(3, 2, GL_FLOAT, false, sizeof(Vertex), (GLfloat*)0 + 10);
		glVertexAttribPointer(4, 3, GL_FLOAT, false, sizeof(Vertex), (GLfloat*)0 + 12);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->GetIBO());
		glBindVertexArray(0);
	}
	Assert(glGetError() == GL_NO_ERROR);
}

void PBRMaterial::RenderShadows()
{
	APBRMaterialTemplate::RenderShadows();

	if (_mesh && _mesh->GetIndicesCount())
	{
		glBindVertexArray(_vao);

		glDrawElements(GL_TRIANGLES, _mesh->GetIndicesCount(), GL_UNSIGNED_INT, 0);
	}
}

void PBRMaterial::Render(DrawContextData* data, int pass)
{
	APBRMaterialTemplate::Render(data, pass);

	if (!IsValid()) 
		return;
	
	VRAssert(pass < 1);
	OpenGLShader* shader = static_cast<OpenGLShader*>(GetShader(pass));

	if (_mesh && _mesh->GetIndicesCount())
	{
		if (_persistentBoard->QuickGetBoolVal("Wireframe"))
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		GLint modelLoc = glGetUniformLocation(shader->GetProgram(), "model");
		GLint normalMatrixLoc = glGetUniformLocation(shader->GetProgram(), "normalMatrix");
		GLint materialLocs[] =
		{
			glGetUniformLocation(shader->GetProgram(), "mat.ambient"),
			glGetUniformLocation(shader->GetProgram(), "mat.albedo"),
			glGetUniformLocation(shader->GetProgram(), "mat.specular"),
			glGetUniformLocation(shader->GetProgram(), "mat.roughness"),
			glGetUniformLocation(shader->GetProgram(), "mat.metallic"),
		};

		glUniformMatrix4fv(glGetUniformLocation(shader->GetProgram(), "M"), 1, false, glm::value_ptr(_model));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(_model));
		glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(_model))));

		float value(1);
		glUniform1f(materialLocs[3], static_cast<float>(_persistentBoard->QuickGetDoubleVal("Roughness")));
		glUniform1f(materialLocs[4], static_cast<float>(_persistentBoard->QuickGetDoubleVal("Metallic")));

		glUniform1ui(glGetUniformLocation(shader->GetProgram(), "environmentSampleCount"), static_cast<GLuint>(_persistentBoard->QuickGetIntVal("EnvironmentSampleCount")));

		/// Texture
		auto locTexAlbedo = glGetUniformLocation(shader->GetProgram(), "texAlbedo");
		auto locTexSpecular = glGetUniformLocation(shader->GetProgram(), "texSpecular");
		auto locTexGlossiness = glGetUniformLocation(shader->GetProgram(), "texGlossiness");
		auto locTexNormal = glGetUniformLocation(shader->GetProgram(), "texNormal");

		BindTexture(locTexAlbedo, _texAlbedo, 0);
		BindTexture(locTexSpecular, _texSpecular, 1);
		BindTexture(locTexGlossiness, _texGlossiness, 2);
		BindTexture(locTexNormal, _texNormal, 3);

		/// Render
		glBindVertexArray(_vao);

		glDrawElements(GL_TRIANGLES, _mesh->GetIndicesCount(), GL_UNSIGNED_INT, 0);
		/// Restore default
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void  PBRMaterial::BindTexture(GLint location, Texture* texture, GLint textureIndex) const
{
	if (texture)
	{
		glUniform1i(location, textureIndex);
		glActiveTexture(GL_TEXTURE0 + textureIndex);
		glBindTexture(GL_TEXTURE_2D, static_cast<OpenGLTexture*>(texture)->GetTexture());
	}
}