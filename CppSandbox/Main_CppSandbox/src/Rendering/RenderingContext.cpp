#include "Rendering/RenderingContext.hpp"

#include "Debug.hpp"

RenderingContext* RenderingContext::_currentContext = nullptr;

bool RenderingContext::sIsInitialized()
{
	RenderingContext* context = sGetContext();
	if (!context)
		return false;
	return context->IsInitialized();
}

unsigned int RenderingContext::sGetWidth()
{
	RenderingContext* context = sGetContext();
	RAssert(context, 0);
	return context->GetWidth();
}

unsigned int  RenderingContext::sGetHeight()
{
	RenderingContext* context = sGetContext();
	RAssert(context, 0);
	return context->GetHeight();
}

void	RenderingContext::RegisterMaterial(Material* newMaterial)
{
	VRAssert(newMaterial);

	if (!IsMaterialRegistered(newMaterial))
		_materials.push_back(newMaterial);
}

bool	RenderingContext::UnregisterMaterial(Material* material)
{
	RAssert(material, false);

	if (IsMaterialRegistered(material))
	{
		_materials.remove(material);
		return true;
	}
	return false;
}

bool	RenderingContext::IsMaterialRegistered(Material* material)
{
	RAssert(material, false);

	auto res = std::find(_materials.begin(), _materials.end(), material);
	if (res == _materials.end())
		return false;
	else
		return true;
}
