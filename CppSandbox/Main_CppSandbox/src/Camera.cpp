#include "Camera.hpp"

#include "imgui.h"
#include "glm/glm.hpp"
#include "glm/gtx/projection.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include "PersistentBoard.hpp"

#include "Rendering/RenderingContext.hpp"

Camera::Camera(std::string const& name)
	: Object(name)
{
}

Camera::~Camera()
{
}

void Camera::GetViewProjection(glm::mat4& view, glm::mat4& projection) const
{
	projection = GetProjection();
	view = glm::lookAt(pos, pos + front, up);
}

glm::mat4 Camera::GetProjection() const
{
	return glm::perspective(
		static_cast<float>(GetPersistantBoard()->QuickGetDoubleVal("FOV")), 
		static_cast<float>(RenderingContext::sGetWidth()) / static_cast<float>(RenderingContext::sGetHeight()),
		static_cast<float>(GetPersistantBoard()->QuickGetDoubleVal("Near")),
		static_cast<float>(GetPersistantBoard()->QuickGetDoubleVal("Far")));
}