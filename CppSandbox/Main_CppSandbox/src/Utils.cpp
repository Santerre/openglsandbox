#include "Utils.hpp"

#include <algorithm>
#include <sstream>

#include <glm/glm.hpp>

namespace Utils
{

namespace Strings
{

std::string ToLower(std::string const& str)
{
	std::string res = str;
	std::transform(res.begin(), res.end(), res.begin(), ::tolower);
	return res;
}

} // namespace Strings

} // namespace Utils