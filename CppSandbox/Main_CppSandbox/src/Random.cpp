#include "Random.hpp"

#include <time.h>
#include "Debug.hpp"

Random::Random()
{
	
}

Random::~Random()
{
}

Random& Random::Get()
{
	static Random rand;
	return rand;
}

void Random::Initialize()
{

}

float Random::GetGaussianSample(float mean, float sDev)
{
	auto distribution = std::normal_distribution<float>(mean, sDev);
	return distribution(generator);
}

void Random::GaussianNoiseArray(float* outData, size_t length, float mean, float sDev)
{
	VRAssert(outData);
	auto distribution = std::normal_distribution<float>(mean, sDev);
	for (size_t i = 0; i < length; i++)
		outData[i] = distribution(generator);
}