#include "TessellatedPlane.hpp"

#include "Debug.hpp"

TessellatedPlane::TessellatedPlane()
{
}

TessellatedPlane::TessellatedPlane(unsigned int tessellationLevel)
{
	UpdateMesh(tessellationLevel);
}


TessellatedPlane::~TessellatedPlane()
{
}

bool TessellatedPlane::UpdateMesh(unsigned int tessellationLevel)
{
	if (_currentTessellation == tessellationLevel)
		return true;

	if (tessellationLevel < 1)
		return false;

	const uint32_t verticesDataCount = (tessellationLevel + 1u) * (tessellationLevel + 1u);
	float* verticesData = new float[2.f * verticesDataCount];
	const uint32_t indicesCount = (tessellationLevel) * (tessellationLevel) * 4;
	uint32_t* indices = new uint32_t[indicesCount];

	int iV = 0;
	float step = 1.f / (tessellationLevel);
	for (float x = 0.f; x < 1.f + 0.0001f; x += step)
		for (float z = 0.f; z < 1.f + 0.0001f; z += step)
		{
			verticesData[iV++] = x;
			verticesData[iV++] = z;
		}

	unsigned int iI = 0u;
	for (uint32_t i = 0u; i < verticesDataCount - tessellationLevel - 2u; i++)
	{
		if (i % (tessellationLevel + 1u) == tessellationLevel)
			i++;
		indices[iI++] = i + 0u;
		indices[iI++] = i + 1u;
		indices[iI++] = i + tessellationLevel + 1u;
		indices[iI++] = i + tessellationLevel + 2u;
	}

	glBindBuffer(GL_ARRAY_BUFFER, GetVBO());
	glBufferData(GL_ARRAY_BUFFER, verticesDataCount * 2u * sizeof(GLfloat), verticesData, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GetIBO());
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesCount * sizeof(GLuint), indices, GL_STATIC_DRAW);

	delete[] verticesData;
	delete[] indices;

	/// May be caused by non initialized buffers.
	RAssert(glGetError() == GL_NO_ERROR, false);

	SetVerticesCount(verticesDataCount);
	SetIndicesCount(indicesCount);

	_currentTessellation = tessellationLevel;

	return true;
}
