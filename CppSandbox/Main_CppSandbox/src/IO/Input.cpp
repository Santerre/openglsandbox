#include "IO/Input.hpp"

#include "Debug.hpp"

Input* Input::_current = nullptr;

Input* Input::GetCurrent()
{
	return _current;
}

void Input::SetCurrent(Input* current)
{
	_current = current;
}

bool Input::TestValidity() const
{
	Input* current = GetCurrent();

	if (current == nullptr)
	{
		ELog("You tried to use uninitialized input class!");
		return false;
	}

	if (current != this)
	{
		ELog("The Input you are using is not valid as another is already bound as current.");
		return false;
	}
	return true;
}