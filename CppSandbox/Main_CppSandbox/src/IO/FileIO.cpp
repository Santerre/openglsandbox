#include "IO/FileIO.hpp"

#include <vector>
#include <fstream>

#include "Debug.hpp"

#ifdef _WIN32
#include "IO/Windows/WinFileIO.hpp"
#elif _ORBIS
#include "IO/PS4/PS4FileIO.hpp"
#endif

namespace FileIO
{

std::vector<std::string> GetFoldersInDirectory(const char* path)
{
#ifdef _WIN32
	return WinFileIO::GetFoldersInDirectory(path);
#elif _ORBIS
	return PS4FileIO::GetFoldersInDirectory(path);
#else
	RAssert(0, std::vector<std::string>());
#endif
}

std::vector<std::string> GetFilesInDirectory(const char* path)
{
#ifdef _WIN32
	return WinFileIO::GetFilesInDirectory(path);
#elif _ORBIS
	return PS4FileIO::GetFilesInDirectory(path);
#else
	RAssert(0, std::vector<std::string>());
#endif
}

bool FileExists(const char* path)
{
#ifdef _WIN32
	return WinFileIO::FileExists(path);
#elif _ORBIS
	return PS4FileIO::FileExists(path);
#else
	RAssert(0, false);
#endif
}

std::string GetExtention(std::string const& file)
{
	auto index = file.find_last_of('.');
	if (index == std::string::npos || index == file.length())
		return "";

	return file.substr(index + 1);
}

std::string ChangeExtention(std::string const& file, std::string const& ext)
{
	auto index = file.find_last_of('.');
	if (index == std::string::npos || index == file.length())
		return file;

	return file.substr(0, index) + ext;
}

std::string GetName(std::string const& file)
{
	std::string tempFile = file;

	auto index = tempFile.find_last_of('/');
	auto size = tempFile.size();

	// Folder case 
	if (index == size - 1)
	{
		if (size == 1)
			return "";
		tempFile = tempFile.substr(0, size - 1);
		index = tempFile.find_last_of('/');
		size--;
	}

	// If no "/"
	if (index == std::string::npos)
		return tempFile;

	if (size == 1)
		return "";

	return tempFile.substr(index + 1);
}

std::string TransformToCurrentPlateform(std::string const& path)
{
#ifdef _WIN32
#elif _ORBIS
	return PS4FileIO::TransformToCurrentPlateform(path);
#else
	RAssert(0, std::vector<std::string>());
#endif
	return path;
}

} // namespace FileIO