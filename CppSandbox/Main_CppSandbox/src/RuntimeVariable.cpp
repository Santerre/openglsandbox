#include "RuntimeVariable.hpp"

#include "Debug.hpp"
#include "Game.hpp"

RuntimeVariable::~RuntimeVariable()
{
	if (_data)
		delete _data;
}

#define QUICK_GET_VAL_DECLARE(type, type_nice_name) type RuntimeVariable::Get##type_nice_name##Value() const\
{\
	if (_data)\
		return *reinterpret_cast<type*>(_data);\
	else\
		return type();\
}

QUICK_GET_VAL_DECLARE(int, Int)
QUICK_GET_VAL_DECLARE(double, Double)
QUICK_GET_VAL_DECLARE(std::string, String)
QUICK_GET_VAL_DECLARE(bool, Bool)
QUICK_GET_VAL_DECLARE(glm::vec3, Vec3)
QUICK_GET_VAL_DECLARE(glm::vec4, Vec4)
QUICK_GET_VAL_DECLARE(ObjectHandle, ObjectHandle)
#undef QUICK_GET_VAL_DECLARE

Object* RuntimeVariable::GetObjectValue() const
{
	if (_data)
	{
		auto obj = reinterpret_cast<ObjectHandle*>(_data);
		return obj->GetObject();
	}
	else
		return nullptr;
}


void const* RuntimeVariable::GetRawValue() const
{
	return _data;
}

void RuntimeVariable::CopyDataFrom(RuntimeVariable const& value)
{
	if (_data)
	{
		delete _data;
		_data = nullptr;
	}
	_data = new char[value._size];
	memcpy(_data, value._data, _size);
	_size = value._size;
}

#define DEFINE_GET_TYPE_ENUM_FUNC(ctype, enumType)\
	template <>\
	RuntimeVariable::Type RuntimeVariable::GetTypeEnum<ctype>()\
	{\
		return RuntimeVariable::Type::enumType;\
	}

DEFINE_GET_TYPE_ENUM_FUNC(int, INT)
DEFINE_GET_TYPE_ENUM_FUNC(double, DOUBLE)
DEFINE_GET_TYPE_ENUM_FUNC(std::string, STRING)
DEFINE_GET_TYPE_ENUM_FUNC(bool, BOOL)
DEFINE_GET_TYPE_ENUM_FUNC(glm::vec3, VEC3)
DEFINE_GET_TYPE_ENUM_FUNC(glm::vec4, VEC4)
#undef DEFINE_GET_TYPE_ENUM_FUNC