#include "Editor/Editor.hpp"

#include "imgui.h"
#ifdef _WIN32
#include "imgui_impl_glfw_gl3.h"
#include "Rendering/OpenGL/OpenGLRenderingContext.hpp"
#elif _ORBIS
#else
Unimplemented platform.
#endif

#include "Debug.hpp"

#include "IO/Input.hpp"

Editor& Editor::GetInstance()
{
	static Editor editor;
	return editor;
}

void Editor::SetEnabled(bool enabled)
{
	_enabled = enabled;
	INPUT->DoHideCursor(!_enabled);
}

void Editor::Toggle()
{
	SetEnabled(!IsEnabled());
}

void Editor::Initialize()
{
#ifdef _WIN32
	ImGui_ImplGlfwGL3_Init(OpenGLRenderingContext::sGetGLFWWindow(), false);
#elif _ORBIS
	WLog("Editor not implemented yet");
#else
	Unimplemented platform.
#endif
}

void Editor::NewFrame()
{
	if (INPUT->DebugAction.WasTriggered())
		Toggle();

#ifdef _WIN32
	ImGui_ImplGlfwGL3_NewFrame();
#elif _ORBIS
#else
	Unimplemented platform.
#endif
}

void Editor::Shutdown()
{
#ifdef _WIN32
	ImGui_ImplGlfwGL3_Shutdown();
#elif _ORBIS
#else
	Not Implemented
#endif
}
