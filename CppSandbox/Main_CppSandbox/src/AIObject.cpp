#include "AIObject.hpp"

#include <glm/gtc/type_ptr.hpp>

#include "Game.hpp"
#include "Rendering/Shader.hpp"
#include "imgui.h"

#include "Rendering/Resources/Mesh.hpp"
#include "Rendering/RenderingContext.hpp"

AIObject::AIObject(GameObject* parent, aiNode const* node)
:	GameObject(node->mName.C_Str(), parent),
	_node(node)
{
	glm::mat4 newTransformMat;
	memcpy(glm::value_ptr(newTransformMat), node->mTransformation[0], 16 * sizeof(float));
	_transform.SetTransformMatrix(newTransformMat);

	for (unsigned int i = 0; i < node->mNumChildren; i++)
		new AIObject(this, node->mChildren[i]);

	if (GetAiNode()->mNumMeshes > 0)
	{
		BaseMaterial.GetRV().SetValue(new PBRMaterial(GetFullName() + "##Material", this));
		RenderingContext::sGetContext()->RegisterMaterial(BaseMaterial);
	}
	auto scene = GAME.scene->GetAiScene();
	for (unsigned int i = 0; i < GetAiNode()->mNumMeshes; i++)
		LoadMesh(scene->mMeshes[GetAiNode()->mMeshes[i]]);
}

AIObject::~AIObject()
{
	for (auto node : _children)
		delete node;
}

void AIObject::Initialize()
{
	GameObject::Initialize();
	///// Texture
	//aiMaterial& aiMaterial = *GetAIMaterial();
	//aiString path;
	//aiMaterial.GetTexture(aiTextureType_DIFFUSE, 0, &path, nullptr, nullptr, nullptr, nullptr);
	//PBRMaterial* mat = static_cast<PBRMaterial*>(BaseMaterial.GetValue());
	//RAssert(BaseMaterial.GetValue());
	//mat->texAlbedo = GAME.resourceManager.SafeLoadTexture(path.C_Str());

	//aiMaterial.GetTexture(aiTextureType_SPECULAR, 0, &path, nullptr, nullptr, nullptr, nullptr);
	//texSpecular = GAME.resourceManager.SafeLoadTexture(path.C_Str());
}

void AIObject::GenIndicesBuffer(aiMesh const* mesh, uint32_t*& buffer, int& count) const
{
	count = 0;
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		count += mesh->mFaces[i].mNumIndices;
	buffer = new uint32_t[count];

	int bufferPos = 0;
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		for (unsigned int j = 0; j < mesh->mFaces[i].mNumIndices; j++)
		{
			buffer[bufferPos] = mesh->mFaces[i].mIndices[j];
			bufferPos++;
		}
	}
}

aiMaterial* AIObject::GetAIMaterial(int meshIndex) const
{
	return GAME.scene->GetAiScene()->mMaterials[GAME.scene->GetAiScene()->mMeshes[GetAiNode()->mMeshes[meshIndex]]->mMaterialIndex];
}

void AIObject::LoadMesh(aiMesh const* mesh)
{
	int vertexSize = 15;
	float* vertex = new float[mesh->mNumVertices * vertexSize];

	for (unsigned int i = 0; i < mesh->mNumVertices * vertexSize;)
	{
		memcpy(vertex + i, &mesh->mVertices[i / vertexSize].x, 3 * sizeof(float));
		i += 3;
		memcpy(vertex + i, mesh->mNormals ? &mesh->mNormals[i / vertexSize].x : glm::value_ptr(glm::vec3(1, 0, 1)), 3 * sizeof(float));
		i += 3;
		memcpy(vertex + i, mesh->mColors[0] ? &mesh->mColors[0][i / vertexSize].r : glm::value_ptr(glm::vec4(0.8f, 0.8f, 0.8f, 1.0f)), 4 * sizeof(float));
		i += 4;
		if (mesh->mTextureCoords[0])
		{
			auto tCoord = mesh->mTextureCoords[0][i / vertexSize];
			memcpy(vertex + i, glm::value_ptr(glm::vec2(tCoord.x, tCoord.y)), 2 * sizeof(float));
		}
		else
		{
			memcpy(vertex + i, glm::value_ptr(glm::vec2(1.f, 1.f)), 2 * sizeof(float));
		}
		i += 2;
		memcpy(vertex + i, mesh->mTangents ? &mesh->mTangents[i / vertexSize].x : glm::value_ptr(glm::vec3(1, 0, 1)), 3 * sizeof(float));
		i += 3;
	}

	_mesh = GAME.resourceManager.CreateMesh();
	_mesh->LoadData(reinterpret_cast<Vertex*>(vertex), mesh->mNumVertices);
	delete[] vertex;

	/// Indices
	uint32_t* indexBuffer;
	int indicesCount;
	GenIndicesBuffer(mesh, indexBuffer, indicesCount);
	_mesh->LoadIndices(indexBuffer, indicesCount);
	delete[] indexBuffer;

	if (BaseMaterial)
		static_cast<PBRMaterial*>(BaseMaterial.GetValue())->SetMesh(_mesh);
}

void AIObject::PreRender()
{
	GameObject::PreRender();

	/*aiMaterial* material = GetAIMaterial();
	aiColor3D color(1, 1, 1);
	material->Get(AI_MATKEY_COLOR_AMBIENT, color);
	glUniform3fv(materialLocs[0], 1, &color.r);
	color = { 1, 1, 1 };
	material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
	glUniform3fv(materialLocs[1], 1, &color.r);
	color = { 1, 1, 1 };
	glUniform3fv(materialLocs[2], 1, &color.r);*/

	static_cast<PBRMaterial*>(BaseMaterial.GetValue())->SetModel(glm::transpose(GetWorldTransformation()));
}