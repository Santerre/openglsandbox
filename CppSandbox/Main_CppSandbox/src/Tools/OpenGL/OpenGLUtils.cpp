#include "GL/glew.h"

#include "Tools/OpenGL/OpenGLUtils.hpp"

#include "glm/gtc/type_ptr.hpp"
#include "Debug.hpp"

namespace OpenGLUtils
{
	void AddGLMUniform(GLint location, int value)
	{
		glUniform1i(location, value);
	}

	void AddGLMUniform(GLint location, float value)
	{
		glUniform1f(location, value);
	}

	void AddGLMUniform(GLint location, glm::vec2 const& value)
	{
		glUniform2f(location, value.x, value.y);
	}

	void AddGLMUniform(GLint location, glm::vec3 const& value)
	{
		glUniform3f(location, value.x, value.y, value.z);
	}

	void AddGLMUniform(GLint location, glm::vec4 const& value)
	{
		glUniform4f(location, value.x, value.y, value.z, value.w);
	}

	void AddGLMUniform(GLint location, glm::mat4 const& value)
	{
		glUniformMatrix4fv(location, 1, false, glm::value_ptr(value));
	}
} // OpenGLUtils