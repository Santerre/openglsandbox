#include "GLFW/glfw3.h"

#include "Tools/Time.hpp"

double Time::GetTimeInSec()
{
	return glfwGetTime();
}
