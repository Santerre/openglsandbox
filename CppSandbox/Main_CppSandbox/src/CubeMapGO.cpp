#include "CubeMapGO.hpp"

#include "Rendering/Materials/CubemapMaterial.hpp"

#include "Rendering/RenderingContext.hpp"

CubeMapGO::CubeMapGO(std::string const& name, GameObject* parent)
:GameObject(name, parent)
{
	BaseMaterial.GetRV().SetValue(new CubemapMaterial(GetName() + "##Material", this), true);
	auto res = BaseMaterial.GetValue();
	RenderingContext::sGetContext()->RegisterMaterial(BaseMaterial);
}

CubeMapGO::~CubeMapGO()
{
}
