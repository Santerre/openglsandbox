#include "Asset.hpp"

#include <algorithm>
#include <string>
#include "Debug.hpp"
#include "Utils.hpp"
#include "IO/FileIO.hpp"

Asset::Asset(Type type, std::string const& assetPath)
:	Object(FileIO::GetName(assetPath)),
	_type(type),
	_assetPath(assetPath)
{
	_debugAuto = false;
}

Asset::~Asset()
{
}

std::string Asset::GetFullName()
{
	return _assetPath;
}

/// Return a corresponding type to the type following the model Type::TYPENAME -> "Typename"
std::string Asset::TypeToString(Type type)
{
	switch (type)
	{
		case Type::UNKNOWN:
			return "Unknown";
		case Type::IMAGE:
			return "Image";
		case Type::SHADER:
			return "Shader";
		case Type::MODEL:
			return "Model";
		default:
			Assert("The type is not defined in the type->string table" == 0);
			return "Unknown";
	}
}

/// Converts a string to the conresponding type. It ignores string case.
/// Returns Type::UNKNOWN if the type as no corresponding type.
Asset::Type Asset::StringToType(std::string const& strType)
{
	std::string lcStrTypr = Utils::Strings::ToLower(strType);

	if (lcStrTypr == "unknown")
		return Type::UNKNOWN;
	if (lcStrTypr == "image")
		return Type::IMAGE;
	if (lcStrTypr == "shader")
		return Type::SHADER;
	if (lcStrTypr == "model")
		return Type::MODEL;

	ELog("The string %s is not defined in the string->type table", lcStrTypr.c_str());
	return Type::UNKNOWN;
}
