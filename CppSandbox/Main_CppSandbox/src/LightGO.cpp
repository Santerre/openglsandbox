#include "LightGO.hpp"

#include "PersistentBoard.hpp"

#include "imgui.h"

LightGO::LightGO(std::string const& name)
: Object(name)
{
	_persistentBoard->AddVal(RuntimeVariable::Allocate("Color", glm::vec4(1, 1, 1, 0)));
}


LightGO::~LightGO()
{
}

void LightGO::DebugUpdate()
{
	Object::DebugUpdate();
}
