#include "Patch.hpp"

#include <random>
#include <time.h>

#include "Rendering/Shader.hpp"
#include "imgui.h"

#include "Rendering/Resources/Mesh.hpp"

Patch::Patch(std::string const& name, GameObject* parent)
: GameObject(name, parent)
{
	_persistentBoard->AddVal(RuntimeVariable::Allocate("DrawPoints", false));

	_persistentBoard->AddVal(RuntimeVariable::Allocate("Tesselation", 20));
	_persistentBoard->AddVal(RuntimeVariable::Allocate("Wireframe", true));

	srand(100);

	for (int i = 0; i < _pointX; i++)
	{
		for (int j = 0; j < _pointY; j++)
		{
			auto pointVar = RuntimeVariable::Allocate("Point" + std::to_string(i * _pointX + j + 1), glm::vec3(-100 + i * 15, rand() % 30 - 15, -40 + j * 15));
			_persistentBoard->AddVal(pointVar);
			pointVar->DisplayPrecision = 0.3;
			pointVar->DisplayMin = -100;
			pointVar->DisplayMax = 100;
		}
	}

	_mesh = new Mesh();

	RAssert(_pointCount >= 16);

	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	glBufferData(GL_ARRAY_BUFFER, _pointCount * 3 * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _mesh->GetIBO());

	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
}

Patch::~Patch()
{
	delete _mesh;
}

void Patch::Initialize()
{
	GameObject::Initialize();
	UpdateIBO();
}

void Patch::Update()
{
	GameObject::Update();
}

void Patch::UpdateIBO()
{
	UpdateIBO(_pointCount);
}



void Patch::UpdateIBO(int pointCount)
{
	if (!_mesh)
		return;

	GLuint* iboArray = new GLuint[GetIndexCount(pointCount)];

	int k = 0;
	for (int i = 0; i < _pointX; i++)
		for (int j = 0; j < _pointY; j++)
		{
			iboArray[k++] = GetIndex(i, j);
		}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _mesh->GetIBO());
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, GetIndexCount(pointCount) * sizeof(GLuint), iboArray, GL_STATIC_DRAW);
	delete[] iboArray;
}

GLuint Patch::GetIndexCount() const
{
	return GetIndexCount(_pointCount);
}
GLuint Patch::GetIndexCount(int pointCount) const
{
	if (pointCount < 16)
		return pointCount;
	return (_pointX) * (_pointY);
}

void Patch::Render(Shader const* shader, int pass)
{
	if (!_mesh)
		return;

	if (pass == 1)
	{
		if (_persistentBoard->QuickGetBoolVal("DrawPoints"))
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glBindVertexArray(_vao);
			glPointSize(10);
			glDrawArrays(GL_POINTS, 0, _pointCount);
			glDrawElements(GL_TRIANGLES, GetIndexCount(), GL_UNSIGNED_INT, 0);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glPointSize(1);
		}
		return;
	}
	if (_persistentBoard->QuickGetBoolVal("Wireframe"))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	float* array = PointsToFloatArray();
	glBufferData(GL_ARRAY_BUFFER, _pointCount * 3 * sizeof(float), array, GL_DYNAMIC_DRAW);
	delete[] array;
	glBindVertexArray(_vao);
	glPatchParameteri(GL_PATCH_VERTICES, 16);
	int tesselation = _persistentBoard->QuickGetIntVal("Tesselation");
	float values[] = { tesselation, tesselation };
	glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, values);
	float values2[] = { tesselation, tesselation, tesselation, tesselation };
	glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, values2);
	glDrawElements(GL_PATCHES, GetIndexCount(), GL_UNSIGNED_INT, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

float* Patch::PointsToFloatArray() const
{
	float* result = new float[_pointCount * 3];
	for (int i = 0; i < _pointCount; i++)
	{
		auto value = _persistentBoard->QuickGetVec3Val("Point" + std::to_string(i+1));
		result[i * 3] = value.x;
		result[i * 3 + 1] = value.y;
		result[i * 3 + 2] = value.z;
	}
	return result;
}
