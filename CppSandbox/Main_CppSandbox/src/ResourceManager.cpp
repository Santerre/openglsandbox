#include "ResourceManager.hpp"

#include <algorithm>

#include "Debug.hpp"
#include "ResourceThread.hpp"
#include "IO/FileIO.hpp"

#ifdef _WIN32
#include "Rendering/Resources/OpenGL/OpenGLTexture.hpp"
#include "Rendering/Resources/OpenGL/OpenGLMesh.hpp"
#elif _ORBIS
#include "Rendering/Resources/PS4/PS4Texture.hpp"
#include "Rendering/Resources/PS4/PS4Mesh.hpp"
#else
Platform Not Implemented
#endif

ResourceManager::~ResourceManager()
{
	JoinThreads();
}

Texture* ResourceManager::CreateTexture()
{
	Texture* texture = nullptr;
#ifdef _WIN32
	texture = new OpenGLTexture();
#elif _ORBIS
	texture = new PS4Texture();
#else
	Platform Not Implemented
#endif
	return texture;
}

Mesh* ResourceManager::CreateMesh()
{
	Mesh* mesh = nullptr;
#ifdef _WIN32
	mesh = new OpenGLMesh();
#elif _ORBIS
	mesh = new PS4Mesh();
#else
	Platform Not Implemented
#endif
	return mesh;
}

Texture* ResourceManager::LoadTexture(std::string const& path, bool linearSpace)
{
	if (IsTextureLoaded(path))
		return GetTexture(path);

	Texture* texture = CreateTexture();
	RAssert(texture, nullptr);
	texture->LoadImage(path, linearSpace);
	return AddToTextureMap(path, texture);
}

Texture* ResourceManager::LoadTextureAsync(std::string const& path, bool linearSpace)
{
	if (IsTextureLoaded(path))
		return GetTexture(path);

	return AddToTextureMap(path, GenTextureAsync(path, linearSpace));
}

Texture* ResourceManager::LoadCubemap(std::string const& path)
{
	if (IsTextureLoaded(path))
		return GetTexture(path);

	Texture* texture = CreateTexture();
	RAssert(texture, nullptr);
	texture->LoadCubemap(path);
	return AddToTextureMap(path, texture);
}

Texture* ResourceManager::GetTexture(std::string const& path) const
{
	RAssert(_textureMap.find(path) != _textureMap.end(), 0);
	return _textureMap.at(path);
}

bool ResourceManager::IsTextureLoaded(std::string const& path) const
{
	return _textureMap.find(path) != _textureMap.end();
}

Texture* ResourceManager::AddToTextureMap(std::string const& path, Texture* texture, bool forceUpdate)
{
	if (IsTextureLoaded(path))
	{
		if (!forceUpdate)
			return _textureMap[path];
		delete _textureMap[path];
	}

	_textureMap[path] = texture;
	return texture;
}

void ResourceManager::JoinThreads()
{
	for (auto&& thread : _resourceThreads)
	{
		thread->Wait();
		delete thread;
	}
}

void ResourceManager::AddResourceThread(ResourceThread* thread)
{
	_resourceThreads.push_back(thread);
}

Texture* ResourceManager::GenTextureAsync(std::string const& path, bool linearSpace)
{
	if (!FileIO::FileExists(path.c_str()))
		return nullptr;

	Texture* texture = CreateTexture();
	RAssert(texture, nullptr);
	texture->LoadNullImage(2, 2);
	auto var = new ResourceThread();
	var->Texture = texture;
	var->LinearSpace = linearSpace;
	var->Run([texture, var, path] () -> AsyncLoadResult
	{
		return texture->PrepareImageAsync(path, var->TextureSizeXY);
	});
	AddResourceThread(var);

	return texture;
}

void ResourceManager::UpdateThreadsStatus()
{
	for (auto&& thread : _resourceThreads)
	{
		if (thread->IsLoadDone())
		{
			Assert(thread);
			if (!thread)
				continue;
			if (thread->IsLoadSuccess())
				thread->InstallLoadedResource();
			delete thread;
			thread = nullptr;
		}
	}
	
	_resourceThreads.erase(std::remove(_resourceThreads.begin(), _resourceThreads.end(), nullptr), _resourceThreads.end());
}
