#version 440 core

in vec3 out_vertex;
in vec3 out_texcoords;
in vec2 out_normal;

out vec4 color;

uniform samplerCube cubemapSampler;

void main()
{
	color = texture(cubemapSampler, out_texcoords);
}