#version 440 core

struct AmbiantLight
{
	vec4 color;
	float intensity;
};

uniform struct Material
{
	vec3 ambient;
	vec3 albedo;
	vec3 specular;
	float specularExponent;
	float specularStrength;
} mat;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normals;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 uv;

out vec3 out_worldPos;
out vec3 out_normal;
out vec2 out_uv;
out vec4 albedo;
out vec4 out_color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normalMatrix;

uniform AmbiantLight ambiantLight;

vec4 computeAmbiant(AmbiantLight light, vec3 matAmbiant)
{
	return vec4(light.color.xyz * light.intensity /** matAmbiant*/, 1.0);
}

void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	out_worldPos = (model * vec4(position, 1.0)).xyz;
	out_normal = normalize((normalMatrix * vec4(normals, 0.0)).xyz);
	out_uv = uv;
	albedo = color;
	out_color = computeAmbiant(ambiantLight, mat.ambient);
}