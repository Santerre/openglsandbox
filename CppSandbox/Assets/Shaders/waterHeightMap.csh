#version 450

layout(local_size_x = 1, local_size_y = 1) in;

uniform float t;

layout(rg32f) restrict readonly uniform image2D oppH0;
layout(rg32f) restrict readonly uniform image2D h0;

//restrict writeonly uniform image2D heightMap; 

uniform int gridResolution;

layout(std430, binding = 3) buffer complexAmplitudes
{
	vec2 values[];
};

vec2 complexMult(vec2 a, vec2 b)
{
	return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

void main()
{
	ivec2 fGroup = ivec2(gl_NumWorkGroups.xy);
	float f2 = float((fGroup.x / 2 + int(gl_WorkGroupID.x)) % fGroup.x - fGroup.x / 2) / 1000.0 + 0.0000001;
	float f1 = float((fGroup.y / 2 + int(gl_WorkGroupID.y)) % fGroup.y - fGroup.y / 2) / 1000.0 + 0.0000001;

	vec2 K = vec2(f1, f2);

	//vec2 K = max(vec2(gl_WorkGroupID.xy) / 1000.0, 0.000001);
	
	ivec2 texCoords = ivec2(gl_WorkGroupID.xy);
	vec2 h0Val = imageLoad(h0, texCoords).xy;
	vec2 oppH0Val = imageLoad(oppH0, texCoords).xy;
	float omega = sqrt(9.8 * length(K));
	vec2 res = complexMult(h0Val, vec2(cos(omega * t), sin(omega * t)));
	res += complexMult(oppH0Val, vec2(cos(-omega * t), sin(-omega * t)));
	//values[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_NumWorkGroups.x] = vec2(1.f);
	//values[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_NumWorkGroups.x] = gl_WorkGroupID.xy;
	//values[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_NumWorkGroups.x] = h0Val;
	values[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_NumWorkGroups.x] = res;
}
