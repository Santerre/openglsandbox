#version 450

layout(local_size_x = 1, local_size_y = 1) in;
 
//// Output image
//restrict writeonly uniform image2D h0;
//restrict writeonly uniform image2D oppH0;

// Output buffer
layout(std430, binding = 3) buffer outBuffer
{
	vec4 xyH0zwOppH0[];
};

layout(rgba32f) restrict readonly uniform image2D gaussianNoiseTex;

uniform int gridResolution;

uniform float waveHeightFactor;
uniform vec2 windForce;

const float oneOverSqrt2 = 0.70710678;

float ComputePhilipsSpectrum(vec2 waveDir, float waveMagnitude, vec2 windDir, float largestWaveMag, float waveHeightFactor)
{
	float kl = waveMagnitude * largestWaveMag;
	float k2 = waveMagnitude * waveMagnitude;
	float dotKW = abs(dot(waveDir, windDir));
	return waveHeightFactor * (exp(-1.0 / (kl * kl)) / (k2 * k2)) * pow(dotKW, 6) * exp(-k2 * 1 * 1);
}

vec2 ComputeH0(vec2 K, vec2 gaussianRandomNumbers)
{
	float k = length(K);
	float l = length(windForce) * length(windForce)/ 9.8;

	float spectrum = ComputePhilipsSpectrum(normalize(K), k, normalize(windForce), l, waveHeightFactor);
	
	return oneOverSqrt2 * gaussianRandomNumbers * sqrt(spectrum);
}

void main()
{
	ivec2 texCoords = ivec2(gl_WorkGroupID.xy);
	//vec2 K = max(vec2(gl_WorkGroupID.xy) / 1000.0, 0.000001);
	
	ivec2 fGroup = ivec2(gl_NumWorkGroups.xy);
	float f2 = float((fGroup.x / 2 + int(gl_WorkGroupID.x)) % fGroup.x - fGroup.x / 2) / 1000.0 + 0.0000001;
	float f1 = float((fGroup.y / 2 + int(gl_WorkGroupID.y)) % fGroup.y - fGroup.y / 2) / 1000.0 + 0.0000001;
	vec2 K = vec2(f1, f2);
	
	//// Output image
	//imageStore(h0, texCoords, vec4(ComputeH0(K, imageLoad(gaussianNoiseTex, texCoords).rg), 1, 1));
	//imageStore(oppH0, texCoords, vec4((vec2(1, -1) * ComputeH0(-K, imageLoad(gaussianNoiseTex, texCoords).ba)), 1, 1));
	
	// Output buffer
	xyH0zwOppH0[texCoords.x + texCoords.y * fGroup.x] = vec4(ComputeH0(K, imageLoad(gaussianNoiseTex, texCoords).rg), 
										vec2(1, -1) * ComputeH0(-K, imageLoad(gaussianNoiseTex, texCoords).ba));
}