#version 440 core

struct DirectionalLight
{
	vec3 direction;
	vec4 color;
	float intensity;
};

uniform struct Material
{
	vec3 ambient;
	vec3 albedo;
	float metallic;
	float roughness;
} mat;

in vec3 out_worldPos;
in vec4 out_dirLightSpacePos;
in vec3 out_normal;
in vec2 out_uv;
in vec4 albedo;
in vec4 out_color;
in vec3 out_tangent;
in vec3 out_bitangent;

out vec4 finalColor;

uniform DirectionalLight directionalLight;
uniform vec3 viewPos;
uniform uint environmentSampleCount;
uniform float shadowBias;

uniform samplerCube cubemap;

uniform sampler2D texAlbedo;
uniform sampler2D texGlossiness;
uniform sampler2D texSpecular;
uniform sampler2D texNormal;
uniform sampler2D directionalDepthMap;

#define M_PI 3.1415926535897932384626433832795

float Geometry(vec3 n, vec3 w, float k)
{
	float cosNH = max(dot(n, w), 0.0);
	return cosNH / (cosNH * (1.0 - k) + k);
}

float SmithGeometry(vec3 normal, vec3 toView, vec3 lightVec, float roughness)
{
	/*float h = roughness * roughness;*/
	float k = ((roughness + 1.0) * (roughness + 1.0)) / 8.0;

	//IBL version
	//float k = (roughness * roughness) / 2;
	return Geometry(normal, toView, k) * Geometry(normal, lightVec, k);
}


float NormalDistribution(vec3 normal, vec3 halfwayVector, float roughness)
{
	float sqR = roughness  * roughness * roughness * roughness;
	float dotNH = max(dot(normal, halfwayVector), 0.0f);
	float sqDotNH = dotNH * dotNH;
	float attenuation = sqDotNH * (roughness * roughness - 1.0f) + 1.0f;
	return sqR / (M_PI * attenuation * attenuation);
}

vec3 Fresnel(vec3 F0, vec3 halfwayVector, vec3 toView)
{
	return F0 + (1.0 - F0) * pow(1.0 - max(dot(halfwayVector, toView), 0.0), 5.0);
}

float chiGGX(float v)
{
    return v > 0 ? 1 : 0;
}

/*float GGX_Distribution(vec3 n, vec3 h, float alpha)
{
    float NoH = dot(n,h);
    float alpha2 = alpha * alpha * alpha * alpha;
    float NoH2 = NoH * NoH;
    float den = NoH2 * alpha2 + (1 - NoH2);
    return (chiGGX(NoH) * alpha2) / ( M_PI * den * den );
}*/

vec4 ComputeCookTorrance(DirectionalLight incommingLight, vec4 albedo, vec3 normal, Material mat)
{
	normal = normalize(normal);
	vec3 fragToEye = normalize(viewPos - out_worldPos);
	vec3 halfwayVector = normalize(fragToEye + normalize(-incommingLight.direction));

	//float roughness = mat.roughness;
	float roughness = 1 - texture(texGlossiness, out_uv).r;
	float metallic = mat.metallic;

	vec3 lambert = albedo.rgb / M_PI;
	float N = NormalDistribution(normal, halfwayVector, roughness);
	float G = SmithGeometry(normal, fragToEye, -incommingLight.direction, roughness);

	vec3 F0 = vec3(0.04);
	F0 = mix(F0, albedo.rgb, metallic);
	vec3 F = Fresnel(F0, halfwayVector, fragToEye);
	//vec3 F = Fresnel(texture(texSpecular, out_uv).rgb, halfwayVector, fragToEye);
	
	vec3 cookTorrance = (N * G * F)
						/ (4.0 * max(dot(fragToEye, normal), 0.0) * max(dot(-incommingLight.direction, normal), 0.0) + 0.0001);

	vec3 ks = F;
	vec3 kd = (vec3(1.0) - ks) * (1.0 - metallic);
	vec3 finalRadiance = (kd * lambert + cookTorrance) * incommingLight.intensity * incommingLight.color.rgb * max(dot(-incommingLight.direction, normal), 0.0);
	//vec3 finalRadiance = lambert * incommingLight.intensity * incommingLight.color.rgb * max(dot(-incommingLight.direction, normal), 0.0);

	//return vec4(vec3(N), 1.0);
	return vec4(finalRadiance, 1.0);
}

void main()
{
	vec3 normal = texture(texNormal, out_uv).rgb;
	normal = normalize(normal * 2.0 - 1.0);

	mat3 TBN = inverse(transpose(mat3(out_tangent, out_bitangent, out_normal)));
	normal = normalize(TBN * normal);
	vec3 dirLightSpacePos = out_dirLightSpacePos.xyz / out_dirLightSpacePos.w;
	dirLightSpacePos = dirLightSpacePos * 0.5f + 0.5f;

	vec4 textureColor = texture(texAlbedo, vec2(out_uv.x, out_uv.y));
	vec4 mixTexAlbedo = vec4((mat.albedo.rgb * (1 - textureColor.a)) + (textureColor.rgb * textureColor.a), textureColor.a);

	vec3 fragToEye = normalize(viewPos - out_worldPos);
	vec3 halfwayVector = normalize(fragToEye - normalize(directionalLight.direction));
	vec4 albedo = albedo * mixTexAlbedo;
	vec4 lightsColor = ComputeCookTorrance(directionalLight, albedo, normal, mat);
	//lightsColor *= step(texture(directionalDepthMap, dirLightSpacePos.xy).r, dirLightSpacePos.z);

	DirectionalLight cubemapLight;
	vec3 cubemapIncident = normalize(reflect(-fragToEye, normal));
	vec4 cubemapColor = vec4(0.0f);
	float sampleCount = 10;

	//IBL render
	/*
	//vec3 tangent = normalize(normal.yxz * vec3(-1, 1, 0));
	vec3 tangent = normalize(cross(cubemapIncident,  vec3(0, 1, 0)));
	vec3 bitangent = normalize(cross(cubemapIncident, tangent));
	mat3 rotationMat = mat3(
		tangent,
		normal,
		bitangent
	);
	rotationMat = inverse(transpose(rotationMat));
	for (float theta = 0; theta < M_PI / 2f; theta += M_PI / (2f * sampleCount))
	{
		for (float phi = 0; phi < 2*M_PI; phi += 2*M_PI / sampleCount)
		{
			vec3 randVec = vec3(sin(theta) * cos(phi), 
								cos(theta),
								sin(theta) * sin(phi)
								);

			vec3 localCubemapIncident = normalize(rotationMat * vec3(0, 1, 0));
			//vec3 localCubemapIncident = normalize(rotationMat * randVec);
			cubemapLight.color = texture(cubemap, localCubemapIncident);
			cubemapLight.intensity = 1;
			cubemapLight.direction = -normalize(localCubemapIncident);
		
			cubemapColor += ComputeCookTorrance(cubemapLight, albedo, normal, mat);
		}
	}*/

	//Full reflection render
	
	/*vec3 localCubemapIncident = normalize(cubemapIncident);
	cubemapLight.color = texture(cubemap, localCubemapIncident);
	cubemapLight.intensity = 1;
	cubemapLight.direction = -normalize(localCubemapIncident);
		
	cubemapColor = cubemapLight.color * albedo;*/

	//finalColor = cubemapColor/* * M_PI / (sampleCount * sampleCount)*/;
	//finalColor = out_color;
	//finalColor = vec4(normal, 1);
	//finalColor =  albedo * texture(texNormal, out_uv);
		
	//float bias = max(shadowBias * 10.0 * (1.0 - dot(normal, directionalLight.direction)), shadowBias); 
	float bias = shadowBias;
	float shadowValue = dirLightSpacePos.z - bias > texture(directionalDepthMap, dirLightSpacePos.xy).r ? 0.1 : 1.0;
	if (dirLightSpacePos.z >= 1)
		shadowValue = 1.0;
	finalColor = out_color * albedo + lightsColor + cubemapColor / float(sampleCount);
	finalColor *= shadowValue;
	//finalColor = dirLightSpacePos.zzzz;
	//finalColor = texture(directionalDepthMap, dirLightSpacePos.xy).rrrr;
	//finalColor = vec4(vec3(1) - vec3(pow(texture(texGlossiness, out_uv).r, 1f/10f)), 1);
}