#version 450

uniform int textureTiling;
uniform sampler2D heightMap;
uniform sampler2D albedoUp;
uniform sampler2D albedoDown;
uniform sampler2D albedoCliff;

uniform float normUpValue;
uniform float slopeUpCos;
uniform float slopeDownCos;

uniform float upDownBlend;

uniform float cliffUpTexBlend;
uniform float cliffDownTexBlend;

uniform struct
{
	vec3 direction;
	vec4 color;
	float intensity;
} directionalLight;

uniform struct
{
	vec4 color;
	float intensity;
} ambiantLight;


out vec4 color;
in vec2 fsNormCoords;

in vec3 normal;

void main()
{
	vec2 tiledCoords =  fsNormCoords * textureTiling;
	vec4 upColor = texture(albedoUp, tiledCoords);
	vec4 downColor = texture(albedoDown, tiledCoords);
	vec4 cliffColor = texture(albedoCliff, tiledCoords);
	float height = texture(heightMap, fsNormCoords).r;
	upColor = mix(upColor, cliffColor, clamp((1 - dot(normal, vec3(0, 1, 0)) - slopeUpCos + cliffUpTexBlend) / (cliffUpTexBlend * 2.0), 0, 1));
	downColor = mix(downColor, cliffColor, clamp((1 - dot(normal, vec3(0, 1, 0)) - slopeDownCos + cliffDownTexBlend) / (cliffDownTexBlend * 2.0), 0, 1));
	vec4 albedo = mix(downColor, upColor, clamp((height - normUpValue + upDownBlend) / (upDownBlend * 2.0), 0, 1));
	vec4 dirLightColor = directionalLight.color * max(dot(-directionalLight.direction, normal), 0) * directionalLight.intensity * 0.3;
	vec4 ambiantColor = ambiantLight.color * ambiantLight.intensity;

	color = albedo * (dirLightColor + ambiantColor);
	//color = vec4(fsNormCoords, 0, 1);
	//color = vec4(normal, 1);
	//color = vec4(vec3(max(dot(-directionalLight.direction, normal), 0)), 1);
}