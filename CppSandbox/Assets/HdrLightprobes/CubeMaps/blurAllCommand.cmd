for /D %%d in ("*") do (
	mkdir %%~ndBlurred
	cd %%d
	for %%i in (jpg, tga, png, hdr) do (
		magick mogrify -path ../%%~ndBlurred/ -blur 0x8 *.%%i
		for /r %%f in (?.%%i) do (
			dd if=%%f of=temp ibs=6c skip=1
			echo | set /p="#?RADIANCE" > %%f
			type temp >> %%f
			del temp
		)
	)
	cd ../
) 
