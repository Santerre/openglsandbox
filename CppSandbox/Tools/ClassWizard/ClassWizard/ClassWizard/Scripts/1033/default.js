
function OnFinish(selProj, selObj)
{
	try
	{
		var strClassPath = wizard.FindSymbol('CLASS_PATH');
		var strClassName = wizard.FindSymbol('CLASS_NAME');
		var mainProject = null;
		var ps4Project = null;
		var projects = wizard.dte.Solution.Projects;
		for (var i = 1; i < projects.Count; i++)
		{
		    var project = projects.Item(i);
		    if (project.Name == "Main_CppSandbox")
		        mainProject = project;
		    else if (project.Name == "PS4_CppSandbox")
		        ps4Project = project;
		}
		var InfFile = CreateCustomInfFile();
		var filePathNoExt = strClassPath + "/" + strClassName;
		var hppPath = "include" + strClassPath + "/";
		var hppName = strClassPath + ".hpp";
		var cppPath = "src" + strClassPath + "/";
		var cppName = strClassPath + ".cpp";
		var isGeneric = wizard.FindSymbol('CHECKBOX_GENERIC');
		if (isGeneric)
		{
		    AddClassToProj(mainProject, strClassPath + "/", strClassName, InfFile);
		}
		else
		{
		    var isPS4 = wizard.FindSymbol('CHECKBOX_PS4');
		    var isWindows = wizard.FindSymbol('CHECKBOX_WINDOWS');
		    var isOpenGL = wizard.FindSymbol('CHECKBOX_OPENGL');
		    if (isPS4 && ps4Project)
		        AddClassToProj(ps4Project, strClassPath + "/PS4/", "PS4" + strClassName, InfFile);
		    if (isWindows)
		        AddClassToProj(mainProject, strClassPath + "/Windows/", "Win" + strClassName, InfFile);
		    if (isOpenGL)
		        AddClassToProj(mainProject, strClassPath + "/OpenGL/", "OpenGL" + strClassName, InfFile);
		}
		InfFile.Delete();

		if (mainProject)
		    mainProject.Object.Save();
		if (ps4Project)
		    ps4Project.Object.Save();
	}
	catch(e)
	{
		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number;
	}
}

function OnIsCrossplateformGenericChanged()
{
    try {
        document.getElementById("NonCrossPlatformDiv").disabled = this.checked;
    }
    catch (e) {
        throw e;
    }
}

function CreateCustomClass(className, classPath)
{
    try
    {
        
    }
    catch (e) {
        throw e;
    }
}

    function CreateCustomProject(strProjectName, strProjectPath)
    {
        try
        {
            var strProjTemplatePath = wizard.FindSymbol('START_PATH');
            var strProjTemplate = '';
            strProjTemplate = strProjTemplatePath + '\\default.vcxproj';

            var Solution = dte.Solution;
            var strSolutionName = "";
            if (wizard.FindSymbol("CLOSE_SOLUTION"))
            {
                Solution.Close();
                strSolutionName = wizard.FindSymbol("VS_SOLUTION_NAME");
                if (strSolutionName.length)
                {
                    var strSolutionPath = strProjectPath.substr(0, strProjectPath.length - strProjectName.length);
                    Solution.Create(strSolutionPath, strSolutionName);
                }
            }

            var strProjectNameWithExt = '';
            strProjectNameWithExt = strProjectName + '.vcxproj';

            var oTarget = wizard.FindSymbol("TARGET");
            var prj;
            if (wizard.FindSymbol("WIZARD_TYPE") == vsWizardAddSubProject)  // vsWizardAddSubProject
            {
                var prjItem = oTarget.AddFromTemplate(strProjTemplate, strProjectNameWithExt);
                prj = prjItem.SubProject;
            }
            else
            {
                prj = oTarget.AddFromTemplate(strProjTemplate, strProjectPath, strProjectNameWithExt);
            }
            var fxtarget = wizard.FindSymbol("TARGET_FRAMEWORK_VERSION");
            if (fxtarget != null && fxtarget != "")
            {
                fxtarget = fxtarget.split('.', 2);
                if (fxtarget.length == 2)
                    prj.Object.TargetFrameworkVersion = parseInt(fxtarget[0]) * 0x10000 + parseInt(fxtarget[1])
            }
            return prj;
        }
        catch(e)
        {
            throw e;
        }
    }

    function AddFilters(proj)
    {
        try
        {
            // Add the folders to your project
            var strSrcFilter = wizard.FindSymbol('SOURCE_FILTER');
            var group = proj.Object.AddFilter('Source Files');
            group.Filter = strSrcFilter;
        }
        catch(e)
        {
            throw e;
        }
    }

    function AddConfig(proj, strProjectName)
    {
        try
        {
            var config = proj.Object.Configurations('Debug');

            var CLTool = config.Tools('VCCLCompilerTool');
            // TODO: Add compiler settings

            var LinkTool = config.Tools('VCLinkerTool');
            // TODO: Add linker settings

            config = proj.Object.Configurations('Release');

            var CLTool = config.Tools('VCCLCompilerTool');
            // TODO: Add compiler settings

            var LinkTool = config.Tools('VCLinkerTool');
            // TODO: Add linker settings
        }
        catch(e)
        {
            throw e;
        }
    }

    function PchSettings(proj)
    {
        // TODO: specify pch settings
    }

    function DelFile(fso, strWizTempFile)
    {
        try
        {
            if (fso.FileExists(strWizTempFile))
            {
                var tmpFile = fso.GetFile(strWizTempFile);
                tmpFile.Delete();
            }
        }
        catch(e)
        {
            throw e;
        }
    }

    function CreateCustomInfFile()
    {
        try
        {
            var fso, TemplatesFolder, TemplateFiles, strTemplate;
            fso = new ActiveXObject('Scripting.FileSystemObject');

            var TemporaryFolder = 2;
            var tfolder = fso.GetSpecialFolder(TemporaryFolder);
            var strTempFolder = tfolder.Drive + '\\' + tfolder.Name;

            var strWizTempFile = strTempFolder + "\\" + fso.GetTempName();

            var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');
            var strInfFile = strTemplatePath + '\\Templates.inf';
            wizard.RenderTemplate(strInfFile, strWizTempFile);

            var WizTempFile = fso.GetFile(strWizTempFile);
            return WizTempFile;
        }
        catch(e)
        {
            throw e;
        }
    }

    function GetTargetName(strName, strProjectName)
    {
        try
        {
            // TODO: set the name of the rendered file based on the template filename
            var strTarget = strName;

            if (strName == 'readme.txt')
                strTarget = 'ReadMe.txt';

            if (strName == 'sample.txt')
                strTarget = 'Sample.txt';

            return strTarget;
        }
        catch(e)
        {
            throw e;
        }
    }

    function AddFilesToCustomProj(proj, strProjectName, strProjectPath, InfFile)
    {
        try
        {
            var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');

            var strTpl = '';
            var strName = '';

            var strTextStream = InfFile.OpenAsTextStream(1, -2);
            while (!strTextStream.AtEndOfStream)
            {
                strTpl = strTextStream.ReadLine();
                if (strTpl != '')
                {
                    strName = strTpl;
                    var strTarget = GetTargetName(strName, strProjectName);
                    var strTemplate = strTemplatePath + '\\' + strTpl;
                    var strFile = strProjectPath + '\\' + strTarget;

                    var bCopyOnly = false;  //"true" will only copy the file from strTemplate to strTarget without rendering/adding to the project
                    var strExt = strName.substr(strName.lastIndexOf("."));
                    if(strExt==".bmp" || strExt==".ico" || strExt==".gif" || strExt==".rtf" || strExt==".css")
                        bCopyOnly = true;
                    wizard.RenderTemplate(strTemplate, strFile, bCopyOnly);
                    proj.Object.AddFile(strFile);
                }
            }
            strTextStream.Close();
        }
        catch(e)
        {
            throw e;
        }
    }

    function AddClassToProj(proj, strClassPath, strClassName, InfFile) {
        try {
            var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');
            var strTemplateHpp = strTemplatePath + '\\Template.hpp';
            var strTemplateCpp = strTemplatePath + '\\Template.cpp';

            wizard.AddSymbol('FINAL_CLASS_NAME', strClassName)
            wizard.AddSymbol('FINAL_CLASS_PATH', strClassPath)

            var filesCommonPath = strClassPath + strClassName;
            var hppPath = 'include/' + filesCommonPath + '.hpp';
            var cppPath = 'src/' + filesCommonPath + '.cpp';
            wizard.RenderTemplate(strTemplateHpp, proj.Object.ProjectDirectory + hppPath, false);
            wizard.RenderTemplate(strTemplateCpp, proj.Object.ProjectDirectory + cppPath, false);
            proj.Object.AddFile(hppPath);
            proj.Object.AddFile(cppPath);
        }
        catch (e) {
            throw e;
        }
    }